#!/bin/bash

filetocheck=$1

shift

for dir in $@
do
    # FINDING THE LAST GAUGE AND FERMION FILES AS THE ONE WHICH REACH THE HIGHEST NTRAJ
    LASTGAUGEFILE='NOTFOUND'
    LASTGAUGETRAJ=0
    LASTFERMFILE='NOTFOUND'
    LASTFERMTRAJ=0
    for gaugefile in $dir/gauge*
    do
        TEMPTRAJ=$(tail -n 1 $gaugefile | awk '{print $1}') 
        if [ "$TEMPTRAJ" -gt "$LASTGAUGETRAJ" ]
        then
            LASTGAUGETRAJ=$TEMPTRAJ
            LASTGAUGEFILE=$gaugefile
        fi
    done
    for fermfile in $dir/ferm*
    do
        TEMPTRAJ=$(tail -n 1 $fermfile | awk '{print $1}') 
        if [ "$TEMPTRAJ" -gt "$LASTFERMTRAJ" ]
        then
            LASTFERMTRAJ=$TEMPTRAJ
            LASTFERMFILE=$fermfile
        fi
    done
    if [  "$LASTFERMFILE" != "NOTFOUND " ]
    then 
        if [ !  "$(grep $LASTFERMFILE $filetocheck)" ]
        then 
            echo $LASTFERMFILE not present in $filetocheck
        fi
    else
        echo "ISSUE: LASTFERMFILE not found"
    fi

    if [  "$LASTGAUGEFILE" != "NOTFOUND " ]
    then 
        if [ !  "$(grep $LASTGAUGEFILE $filetocheck)" ]
        then 
            echo $LASTGAUGEFILE not present in $filetocheck
        fi
    else
        echo "ISSUE: LASTGAUGEFILE not found"
    fi
done 
