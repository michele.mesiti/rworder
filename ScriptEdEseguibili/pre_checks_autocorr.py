#!/usr/bin/env python3

import numpy as np
import rw_input_file_parser as parser
from sys import argv,exit
from matplotlib import pyplot as plt
import os.path as path


import osservabili_rw as orw
from gauge_and_fermion_correction import *

ifp = parser.analisiSetting(argv[1])

doFermions =  False
doGauge = False
doPlot = True
doThermCheck = True
generalMaxBlockSize = 600
if 'doFermions' in argv:
    doFermions = True
if 'doGauge' in argv:
    doGauge = True


print("doFermions = ",doFermions )
print("doPlot     = ",doPlot     )
print("doGauge    = ",doGauge    )

for beta in sorted(ifp.betaInfo.keys()):
    runGroupInfo = ifp.betaInfo[beta].info
    runRecords   = ifp.betaInfo[beta].runRecords

    gaugeDatas = []
    fermionicDatas = []
    vertShiftF = 0
    vertShiftG = 0
    for runInfo in runRecords:
 
        fermionicFileName = runInfo.fermionicFileName
        stdout.write("Reading {}\n".format(fermionicFileName))
        if runGroupInfo.mode == 'mode2.0_28':
            stringFile = open(runInfo.fermionicFileName,'r').readlines()
            stringFile = stringFile[1:]
            stringFile = '\n'.join(stringFile)
            stringFileCorrected = stringFile.replace('none','nan')
            fermionicData = np.fromstring(stringFileCorrected,sep = '\t').reshape((-1,38))
        else :
            fermionicData = np.loadtxt(fermionicFileName)
    
        fermionicDatas.append(fermionicData)
        
        gaugeFileName = runInfo.gaugeFileName 
        stdout.write("Reading {}\n".format(gaugeFileName    ))
        gaugeData = np.loadtxt(gaugeFileName)
        gaugeDatas.append(gaugeData)
        
        if doPlot and doThermCheck:
            xs = np.array([runInfo.therm,runInfo.therm])
            if runInfo.cutMode == 'fromFileBeginning':
                xs = xs + gaugeData[0,0] # we add the value of the first trajectory 

            plotName = path.basename(path.dirname(runInfo.gaugeFileName))
            plt.figure(1)
            plt.plot(gaugeData[:,0],gaugeData[:,5]+vertShiftG,label = plotName + 'imPoly')
            plt.plot([np.min(gaugeData[:,0]),np.max(gaugeData[:,0])],\
                    [vertShiftG,vertShiftG])
            ys = np.array([np.min(gaugeData[:,5]),np.max(gaugeData[:,5])]) + vertShiftG
            plt.plot(xs,ys,label = 'Cut '+str(beta)+' '+str(runInfo.runIndice))

            vertShiftG += np.std(gaugeData[:,5])
            plt.figure(2)

            plt.plot(fermionicData[:,0],fermionicData[:,2]+vertShiftF,label = plotName + 'upChCond')
            plt.plot([np.min(fermionicData[:,0]),np.max(fermionicData[:,0])],\
                    [vertShiftF,vertShiftF])

            ys = np.array([np.min(fermionicData[:,2]),np.max(fermionicData[:,2])]) + vertShiftF
            plt.plot(xs,ys,label = 'Cut '+str(beta)+' '+str(runInfo.runIndice))

            vertShiftF += np.std(fermionicData[:,2])

        print("beta: {}, mode: {}, thermalization traj {}".format(beta, runGroupInfo.mode, runInfo.therm))
        
    if doPlot and doThermCheck:
        plt.figure(1)
        plt.legend(loc = 'lower right')
        plt.figure(2)
        plt.legend(loc = 'lower right')
        plt.show()

    gaugeData = np.vstack(gaugeDatas)
    fermionicData = np.vstack(fermionicDatas)


    nMeas = gaugeData.shape[0]


    idx = orw.indexDictionary(runGroupInfo.mode)
    orw.verbose = True

    if doGauge:
        for observable in orw.gaugeObs:
            name = str(beta) + " " + observable.__name__
            errors = []
            means = []
            maxBlockSize = min(int(nMeas/4),generalMaxBlockSize)
            blockSizeInterval = max(int(maxBlockSize/50),1)
            blockSizes = list(range(1,maxBlockSize,blockSizeInterval))
            for blockSize in blockSizes: 
                m,e = observable(gaugeData,blockSize,idx)
                means.append(m)
                errors.append(e)
            means = np.array(means)
            errors = np.array(errors)

            if doPlot: 
                plt.errorbar(np.array(blockSizes), np.array(means),\
                        yerr=np.array(errors),label = name)
                xs = np.array([runGroupInfo.block,runGroupInfo.block])
                ys = np.array([np.min(means-errors),np.max(means+errors)])
                plt.plot(xs,ys,label = 'Block '+str(beta) , color = 'orange')
                plt.legend(loc='upper left')
                plt.show()
    

    if doFermions:
        for observable in orw.fermObs:
            name = str(beta) + " " + observable.__name__
            errors = []
            means = []
            blockSizes = list(range(1,min(int(nMeas/4),generalMaxBlockSize),\
                    max(int(min(int(nMeas/4),300)/50),1)))
            for blockSize in blockSizes: 
                m,e = observable(fermionicData,blockSize,\
                        runGroupInfo.nFermCopie,runGroupInfo.volume,idx)
                means.append(m)
                errors.append(e)

            means = np.array(means)
            errors = np.array(errors)
            if doPlot: 
                plt.errorbar(np.array(blockSizes), np.array(means),\
                        yerr=np.array(errors), label = name)
                xs = np.array([runGroupInfo.block,runGroupInfo.block])
                ys = np.array([np.min(means-errors),np.max(means+errors)])
                plt.plot(xs,ys,label = 'Block '+str(beta) , color = 'orange')
                plt.legend(loc='upper left')
                plt.show()
        
    
    
    
    
    
    
