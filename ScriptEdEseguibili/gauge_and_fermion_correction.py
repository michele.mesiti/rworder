#!/usr/bin/env python3

import numpy as np
from sys import stderr,exit,stdout

correctionSuffix = 'CORRECTED'
minimalFixSuffix = 'MINCORR' # only for gauge


def sanitizeFermionicData(fermionicData, nCopies, thermEndP,versionMode, trajsWithProblems):
    # REMOVING EXCESS COPIES FROM DATAFILE
  
    stderr.write("REMOVING COPIES IN EXCESS OF {}\n".format(nCopies))
    condition = fermionicData[:,1] < nCopies
    
    trajsWithProblems = np.array(trajsWithProblems)
    if versionMode == 'mode1.0': # In this case, fermion file measurements were mislabelled
        trajsWithProblems -= 1            
        fermionTherm = thermEndP - 1 # this issue had been corrected in new code  
    else:
        fermionTherm = thermEndP 

    condition = np.logical_and(condition,fermionicData[:,0] > fermionTherm)

    for trajWithProblems in trajsWithProblems:
        condition = np.logical_and(condition, fermionicData[:,0] != trajWithProblems)

    fermionicData = fermionicData[condition, :]
    return fermionicData

def sanitizeGaugeData(gaugeData, trajsWithProblems, thermEndP):
    # REMOVING TRAJS TO SKIP
    stderr.write("CLEANING GAUGE DATA...\n")
 

    condition = gaugeData[:,0] > thermEndP

    for trajWithProblems in trajsWithProblems:
        condition = np.logical_and(condition, gaugeData[:,0] != trajWithProblems)
 
    gaugeData = gaugeData[condition, :]

    return gaugeData



def findProblemsGaugeData(gaugeData, therm, cutMode):
    # CHECKING WHAT'S WRONG
    stderr.write("CHECKING GAUGE DATA FOR PROBLEMS...\n")
    gaugeCheckArray = gaugeData[:,0]

    if cutMode == 'ntrajBased':
        thermEndP = therm
    elif cutMode == 'fromFileBeginning': 
        thermEndP = therm + gaugeData[0,0]
    else:
        stderr.write("Cut mode not recognized!")
        exit(1)

    trajsWithProblems = []
    totalcount = 0
    for itraj in range(int(np.min(gaugeCheckArray)),\
            int(np.max(gaugeCheckArray)+1)):
        condition = (gaugeCheckArray == itraj)
        count = np.sum(condition)
        if count != 1:
            stderr.write("(GAUGE) Probem at traj {}:  {} measures instead of 1. These measures will be skipped\n".format(itraj,count))
            trajsWithProblems.append(itraj)
            totalcount += count
 
    stderr.write("(GAUGE) Lines to skip/total: {}/{}\n".format(totalcount, gaugeData.shape[0]))
    stdout.write("(GAUGE) Lines to skip/total: {}/{}\n".format(totalcount, gaugeData.shape[0]))

    return trajsWithProblems, thermEndP


def findProblemsFermionicData(fermionicData, nCopies, versionMode):
    # REMOVING EXCESS COPIES FROM DATAFILE
  
    stderr.write("REMOVING COPIES IN EXCESS OF {}\n".format(nCopies))
    condition = fermionicData[:,1] < nCopies

    fermionicData = fermionicData[condition,:]
    
    # CHECKING WHAT'S WRONG
    stderr.write("CHECKING FERMIONIC DATA...\n")
    fermionCheckArray = fermionicData[:,:2] 
    trajsWithProblems = []
    totalcount = 0
    for itraj in range(int(np.min(fermionCheckArray[:,0])),\
            int(np.max(fermionCheckArray[:,0]))+1):
        condition = (fermionCheckArray[:,0] == itraj)
        count = np.sum(condition)
        if count != nCopies:
            #stderr.write("(FERMION) Probem at traj {}:  {} measures instead of {}. These measures will be skipped\n".format(itraj,count,nCopies))
            trajsWithProblems.append(itraj)
            totalcount += count
 
    stderr.write("(FERMION) Lines to skip/total: {}/{}\n".format(totalcount, fermionicData.shape[0]))
    stdout.write("(FERMION) Lines to skip/total: {}/{}\n".format(totalcount, fermionicData.shape[0]))

    trajsWithProblems = np.array(trajsWithProblems)
    if versionMode == 'mode1.0': # In this case, fermion file measurements were mislabelled
        trajsWithProblems += 1            

    return trajsWithProblems




def checkGaugeFermConsistency(gaugeData,fermionicData,nFermCopies,versionMode):

    if gaugeData.shape[0] != fermionicData.shape[0]/nFermCopies:
        stdout.write("Number of measurements differ:\n{}\n{}\n "\
                .format(gaugeData.shape[0],fermionicData.shape[0]/nFermCopies))
        stdout.write("First- Last gauge measurement: {} - {} \n"\
                .format(np.min(gaugeData[:,0]), np.max(gaugeData[:,0])))
        stdout.write("First- Last fermion measurement: {} - {} \n"\
                .format(np.min(fermionicData[:,0]), np.max(fermionicData[:,0])))
        return False
    
    if versionMode == 'mode1.0': # In this case, fermion file measurements were mislabelled
        maxDisAlignment = np.max(np.abs(gaugeData[:,0]-(fermionicData[::nFermCopies,0]+1)))
    elif versionMode in [ 'mode2.0' ,'mode2.0Deg2','mode3.0','mode2.0_28'] : # this issue had been corrected in new code  
        maxDisAlignment = np.max(np.abs(gaugeData[:,0]-fermionicData[::nFermCopies,0]))
    else:
        stderr.write("MODE {} NOT RECOGNIZED\n".format(versionMode))
        exit(1)
    
    stdout.write("Disalignment max: {}\n".format(maxDisAlignment))

    return maxDisAlignment == 0  

def alignGaugeAndFermionData(gaugeDataCorrected,fermionDataCorrected,nCopies,versionMode):


    fermionTrajs = np.array(fermionDataCorrected[:,0])
    if versionMode == 'mode1.0': # In this case, fermion file measurements were mislabelled
        stderr.write("Adding +1 for fermion trajectories\n")
        fermionTrajs += 1
    elif versionMode in [ 'mode2.0' ,'mode2.0Deg2','mode3.0','mode2.0_28'] : # this issue had been corrected in new code  
        stderr.write("fermion trajectories should be fine\n")
    else:
        stderr.write("MODE {} NOT RECOGNIZED\n".format(versionMode))
        exit(1)

    gaugeTrajs = set(gaugeDataCorrected[:,0])
    acceptedTrajs = set(fermionTrajs).intersection(gaugeTrajs)

    gCondition = np.array([i in acceptedTrajs for i in gaugeDataCorrected[:,0]])
    if versionMode == 'mode1.0': # In this case, fermion file measurements were mislabelled
        fCondition = np.array([i+1 in acceptedTrajs for i in fermionDataCorrected[:,0]])
    else:
        fCondition = np.array([i in acceptedTrajs for i in fermionDataCorrected[:,0]])


    gaugeDataAligned = gaugeDataCorrected[gCondition, :]
    fermionDataAligned = fermionDataCorrected[fCondition, :]

    if checkGaugeFermConsistency(gaugeDataAligned,fermionDataCorrected,nCopies,versionMode):
        return gaugeDataAligned,fermionDataAligned
    else:    
        stderr.write("FERMION-GAUGE ALIGNMENT WAS NOT ACHIEVED!\n")
        return None


def sanitizeFermionicDataOld(fermionicData, nCopies, therm, cutMode,versionMode):
    # REMOVING EXCESS COPIES FROM DATAFILE
  
    stderr.write("REMOVING COPIES IN EXCESS OF {}\n".format(nCopies))
    condition = fermionicData[:,1] < nCopies

    fermionicData = fermionicData[condition,:]
    
    # CHECKING WHAT'S WRONG
    stderr.write("CHECKING FERMIONIC DATA...\n")
    fermionCheckArray = fermionicData[:,:2] 
    trajsWithProblems = []
    totalcount = 0
    for itraj in set(fermionCheckArray[:,0]):
        condition = (fermionCheckArray[:,0] == itraj)
        count = np.sum(condition)
        if count != nCopies:
            #stderr.write("(FERMION) Probem at traj {}:  {} measures instead of {}. These measures will be skipped\n".format(itraj,count,nCopies))
            trajsWithProblems.append(itraj)
            totalcount += count
 
    stderr.write("Lines to skip/total: {}/{}\n".format(totalcount, fermionicData.shape[0]))

    if versionMode == 'mode1.0': # In this case, fermion file measurements were mislabelled
        fermionTherm = therm - 1 # this issue had been corrected in new code  
    else:
        fermionTherm = therm 

    if cutMode == 'ntrajBased':
        condition = fermionicData[:,0] > fermionTherm
    elif cutMode == 'fromFileBeginning': 
        condition =  fermionicData[:,0] > therm + fermionicData[0,0]
    else:
        stderr.write("Cut mode not recognized!")
        exit(1)

    for trajWithProblems in trajsWithProblems:
        condition = np.logical_and(condition, fermionicData[:,0] != trajWithProblems)

    trajsWithProblems = np.array(trajsWithProblems)
    if versionMode == 'mode1.0': # In this case, fermion file measurements were mislabelled
        trajsWithProblems += 1            

    fermionicData = fermionicData[condition, :]
    return fermionicData, trajsWithProblems


