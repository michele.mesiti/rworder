#!/bin/bash
# command, arguments to command, -> command_hash.log
HASH=$(echo $@ | md5sum | cut -d' ' -f1)
echo $@ > $(basename $1)_$HASH.log
$@ | tee -a $(basename $1)_$HASH.log
