#!/usr/bin/env python3

import numpy as np
import rw_input_file_parser as parser
from sys import argv,exit,stdout, stderr
#from matplotlib import pyplot as plt
from io import StringIO


import osservabili_rw as orw
from gauge_and_fermion_correction import *


stdout.write("Reading {}\n".format(argv[1]))

ifp = parser.analisiSetting(argv[1])

beta = float(argv[2])

if beta not in ifp.betaInfo:
    stdout.write("Beta = {} is not present in setting file.\n".format(beta))
    exit(0)

runGroupInfo = ifp.betaInfo[beta].info
runRecords   = ifp.betaInfo[beta].runRecords

modesThatRequireEpuration = [ 'mode2.0_28' ]

for runInfo in runRecords:
    stdout.write("Reading {}\n".format(runInfo.gaugeFileName))
    gaugeData = np.loadtxt(runInfo.gaugeFileName)
    stdout.write("Reading {}\n".format(runInfo.fermionicFileName))
    if runGroupInfo.mode not in modesThatRequireEpuration: 
        fermionicData = np.loadtxt(runInfo.fermionicFileName)
    elif runGroupInfo.mode == 'mode2.0_28':
        stringFile = open(runInfo.fermionicFileName,'r').readlines()
        stringFile = stringFile[1:]
        stringFile = '\n'.join(stringFile)
        stringFileCorrected = stringFile.replace('none','nan')
        fermionicData = np.fromstring(stringFileCorrected,sep = '\t').reshape((-1,38))
    else:
        stderr.write("MODE NOT RECOGNIZED!\n")
        exit(1)


    # finding problems

    trajsWithProblemsG,thermEndP = findProblemsGaugeData(gaugeData,runInfo.therm,\
            runInfo.cutMode)
    trajsWithProblemsG = set(trajsWithProblemsG)
    trajsWithProblemsF = set(findProblemsFermionicData(fermionicData,\
            runGroupInfo.nFermCopie,runGroupInfo.mode))
    
    commonProblems = trajsWithProblemsG.intersection(trajsWithProblemsF)
    onlyGProblems = trajsWithProblemsG.difference(trajsWithProblemsF)
    onlyFProblems = trajsWithProblemsF.difference(trajsWithProblemsG)

    commonProblems_strings = [ str(tWithPr) for tWithPr in commonProblems]
    onlyGProblems_strings  = [ str(tWithPr) for tWithPr in onlyGProblems ]
    onlyFProblems_strings  = [ str(tWithPr) for tWithPr in onlyFProblems ]

    for stream in stdout,stderr:
        stream.write("\nTrajectories with problems in both fermion and gauge file: {}".\
                format(len(commonProblems_strings)))
        #stream.write(' '.join(commonProblems_strings))
        stream.write("\nTrajectories with problems only in fermion file: {}".\
                format(len(onlyFProblems_strings)))
        #stream.write(' '.join(onlyFProblems_strings))
        stream.write("\nTrajectories with problems only in gauge file: {}".\
                format(len(onlyGProblems_strings)))
        #stream.write(' '.join(onlyGProblems_strings))


    minGaugeTraj = np.min(gaugeData[:,0])
    minFermTraj  = np.min(fermionicData[:,0])
    stderr.write("\nMeasurements start at traj {} for gauge file.".format(minGaugeTraj))
    stderr.write("\nMeasurements start at traj {} for fermion file.".format(minFermTraj))
    if runGroupInfo.mode == 'mode1.0': # In this case, fermion file measurements were mislabelled
        minFermTraj += 1            

    startTraj = max(minFermTraj,minGaugeTraj)

    maxGaugeTraj = np.max(gaugeData[:,0])
    maxFermTraj  = np.max(fermionicData[:,0])
    stderr.write("\nMeasurements end at traj {} for gauge file.".format(maxGaugeTraj))
    stderr.write("\nMeasurements end at traj {} for fermion file.".format(maxFermTraj))
    if runGroupInfo.mode == 'mode1.0': # In this case, fermion file measurements were mislabelled
        maxFermTraj += 1            

    endTraj = min(maxFermTraj,maxGaugeTraj)

    conditionOnGauge = np.logical_and(gaugeData[:,0] >= startTraj, gaugeData[:,0] <= endTraj)

    if runGroupInfo.mode == 'mode1.0': # In this case, fermion file measurements were mislabelled
        conditionOnFerm = np.logical_and(fermionicData[:,0] >= startTraj-1, fermionicData[:,0] <= endTraj-1)
    else :
        conditionOnFerm = np.logical_and(fermionicData[:,0] >= startTraj, fermionicData[:,0] <= endTraj)

    #print(gaugeData.shape)
    gaugeData = gaugeData [ conditionOnGauge, :] 
    #print(gaugeData.shape)
    #print(fermionicData.shape)
    fermionicData = fermionicData [ conditionOnFerm, :] 
    #print(fermionicData.shape)

    stderr.write("\nCorrected fermionic and gauge data with traj limits {} and {}".\
            format(startTraj, endTraj)) 

    stderr.write("\n")
    stdout.write("\n")

    


    trajsWithProblems =  trajsWithProblemsF.union(trajsWithProblemsG)
    trajsWithProblems = list(trajsWithProblems)

    # removing them
    fermionicDataSane = sanitizeFermionicData(fermionicData,runGroupInfo.nFermCopie,
            thermEndP,runGroupInfo.mode,trajsWithProblems)

    # gauge case : removing only trajs with problems in the gauge case
    gaugeDataAlmostSane = sanitizeGaugeData(gaugeData,trajsWithProblemsG,thermEndP)
    stderr.write("beta: {}, mode: {}, thermalization traj {}\n".format(beta, runGroupInfo.mode, runInfo.therm))

    # gauge case : removing also trajs with problems in the fermion case
    gaugeDataSane = sanitizeGaugeData(gaugeData,trajsWithProblems,thermEndP)
    stderr.write("beta: {}, mode: {}, thermalization traj {}\n".format(beta, runGroupInfo.mode, runInfo.therm))

    gaugeDataAlmostSaneFileName = runInfo.gaugeFileName + minimalFixSuffix
    stdout.write("Writing {}\n".format(gaugeDataAlmostSaneFileName))
    np.savetxt(gaugeDataAlmostSaneFileName,gaugeDataAlmostSane)

    gaugeDataSaneFileName = runInfo.gaugeFileName + correctionSuffix 
    stdout.write("Writing {}\n".format(gaugeDataSaneFileName))
    np.savetxt(gaugeDataSaneFileName,gaugeDataSane)
    fermionicDataSaneFileName = runInfo.fermionicFileName + correctionSuffix 
    stdout.write("Writing {}\n".format(fermionicDataSaneFileName))
    np.savetxt(fermionicDataSaneFileName,fermionicDataSane)


