#!/usr/bin/env python3

# script to evaluate in how many chunks to split everything

import glob
import re
import numpy as np
from sys import argv

maxDurationSec = int(argv[1])

def fileNameCutFromFileName(fileName):
    mode = re.search('gauge|ferm',fileName).group()
    mass = re.search('m0.[0-9]{3,5}',fileName).group()
    L    = re.search('L[0-9]{2}',fileName).group()
    return mode+mass+L

def fileNameCutFromFileNameNoMode(fileName):
    mass = re.search('m0.[0-9]{3,5}',fileName).group()
    L    = re.search('L[0-9]{2}',fileName).group()
    return mass+L



fileNames = []
for mode in [ 'ferm','gauge' ]:
    fileNames += glob.glob('out'+mode+'MHRW.analisi_settings_m0.00*')

fileNamesCut = set([ fileNameCutFromFileName(fileName) for fileName in fileNames ])


fileNameDuration = dict()
for fileName in fileNames:
    with open(fileName, 'r') as f:
        lines = f.readlines()
        f.close()
        lines = [ line for line in lines if 'completing in' in line] # grep 
        if len(lines) != 0:
            duration = int(lines[-1].split()[-1][:-1])
            fileNameDuration[fileName] = duration
        else:
            print(fileName + ' has no duration')
        
    
totalDurations = dict()        

for k,v in fileNameDuration.items():
    newk = fileNameCutFromFileName(k)
    if newk in totalDurations:
        totalDurations[newk] += v
    else :
        totalDurations[newk] = v

print("Data: Tot duration, chunks (for duration less than {})".format(maxDurationSec))
for k,v in totalDurations.items():
    nchunks = np.ceil(v/maxDurationSec)
    print('{} : {}, {} (expected {})'.format(k,v,nchunks,v/nchunks))

