# this script can be used for creating the logs and then the makefiles. 

# ../ScriptEdEseguibili/gauge_and_fermion_corrector.py
# of for ../ScriptEdEseguibili/do_histograms.py
# because they act on a single beta value of the input file
#for file in *.set 
#do betas=$(grep -v '#' $file | awk '{print $1}' | uniq )
#    for beta in $betas 
#    do  
#        ../ScriptEdEseguibili/log.sh ../ScriptEdEseguibili/gauge_and_fermion_corrector.py $file $beta
#        ../ScriptEdEseguibili/log.sh ../ScriptEdEseguibili/do_histograms.py $file $beta
#    done
#    ../ScriptEdEseguibili/log.sh ../ScriptEdEseguibili/analisi.py $file
#done
#
#gesucristo.py makefile gauge_and_fermion_corrector.py*.log analisi.py*.log
#gesucristo.py makefile_hist gauge_and_fermion_corrector.py*.log do_histograms.py*.log
#
# to obtain preliminary values 

../ScriptEdEseguibili/log.sh ../ScriptEdEseguibili/analisi_mhrw_gauge.py analisi_settings_m0.00075_L16.set 3.34   3.38   2 
../ScriptEdEseguibili/log.sh ../ScriptEdEseguibili/analisi_mhrw_gauge.py analisi_settings_m0.00075_L20.set 3.35   3.38   2 
../ScriptEdEseguibili/log.sh ../ScriptEdEseguibili/analisi_mhrw_gauge.py analisi_settings_m0.00075_L24.set 3.355  3.385  2  
                                                                                                               
../ScriptEdEseguibili/log.sh  ../ScriptEdEseguibili/analisi_mhrw_gauge.py analisi_settings_m0.0015_L16.set 3.35   3.40   2 
../ScriptEdEseguibili/log.sh  ../ScriptEdEseguibili/analisi_mhrw_gauge.py analisi_settings_m0.0015_L20.set 3.36   3.40   2 
../ScriptEdEseguibili/log.sh  ../ScriptEdEseguibili/analisi_mhrw_gauge.py analisi_settings_m0.0015_L24.set 3.36   3.40   2 
                                                                                                             
../ScriptEdEseguibili/log.sh   ../ScriptEdEseguibili/analisi_mhrw_gauge.py analisi_settings_m0.003_L16.set 3.39   3.43   2 
../ScriptEdEseguibili/log.sh   ../ScriptEdEseguibili/analisi_mhrw_gauge.py analisi_settings_m0.003_L20.set 3.40   3.44   2 
../ScriptEdEseguibili/log.sh   ../ScriptEdEseguibili/analisi_mhrw_gauge.py analisi_settings_m0.003_L24.set 3.40   3.44   2 



