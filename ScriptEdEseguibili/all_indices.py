#!/usr/bin/env python3
# This python library is provided to analyse the data produced by YALQCDSS.

from sys import exit,stderr


class indexDictionary:

    def setIndices(self,mode):
        if mode == 'mode1.0':
            self.setIndices10()
        elif mode == 'mode2.0':
            self.setIndices20()
        elif mode == 'mode2.0Deg2':
            self.setIndices20Deg2()
        elif mode == 'mode2.0_28':
            self.setIndices20()
        elif mode == 'mode3.0':
            self.setIndices30()
        else:
            stderr.write("MODE {} NOT RECOGNIZED!")
            exit(1)


    def __init__(self,mode):
        # Gauge file column dictionary, see src/OpenAcc/main.c
        self.iTraj      = 0 
        self.iAccepted  = 1     
        self.iPlaquette = 2      
        self.iRectangle = 3      
        self.iRePoly    = 4   
        self.iImPoly    = 5   
        
        # Fermion file column dictionary, see src/Meas/ferm_meas.c
        # general
        self.iTraj      = 0 
        self.iCopy      = 1

        self.setIndices(mode)

# Call the right one of these functions BEFORE doing any analysis

    def setIndices30(self):
        # this has not yet been implemented in YALQUCDSS but for 
        # reweiting of the fermionic observables this should be the best 
        # solution (matching lines in gauge and fermion files is messy)
        self.iPlaquetteF           =  2
        self.iRectangleF           =  3      
        # The following are     (Degeneracy/(4 V_4))   Tr< something >
        # up
        self.iUpReChCond           =  4 #     1/(4*V4) Tr[M^{-1}]
        self.iUpImChCond           =  5 # = 0 
        self.iUpReN                =  6 # = 0 1/(4*V4) Tr[dM/dmu M^{-1}]
        self.iUpImN                =  7
        self.iUpReMag              =  8                         
        self.iUpUImag              =  9                         
        self.iUpReChSuscConn       = 10 #     1/(4*V4) Tr[M^{-2}]      
        self.iUpImChSuscConn       = 11 # = 0                                           
        self.iUpReQNSuscConn1      = 12 #     1/(4*V4) Tr[M^{-1}d2M/dmu2]               
        self.iUpImQNSuscConn1      = 13 # = 0                                           
        self.iUpReQNSuscConn2      = 14 #     1/(4*V4) Tr[M^{-1}dM/dmu M^{-1}dM/dmu]    
        self.iUpImQNSuscConn2      = 15 # = 0                                           
        # down                                           
        self.iDownReChCond         = 16 #      1/(4*V4) Tr[M^{-1}]                      
        self.iDownImChCond         = 17 # = 0                                           
        self.iDownReN              = 18 # = 0  1/(4*V4) Tr[dM/dmu M^{-1}]               
        self.iDownImN              = 19                                                 
        self.iDownReMag            = 20                           
        self.iDownUImag            = 21                           
        self.iDownReChSuscConn     = 22 #     -1/(4*V4) Tr[M^{-2}]                      
        self.iDownImChSuscConn     = 23 # = 0                                           
        self.iDownReQNSuscConn1    = 24 #      1/(4*V4) Tr[M^{-1}d2M/dmu2]              
        self.iDownImQNSuscConn1    = 25 # = 0                                           
        self.iDownReQNSuscConn2    = 26 #     -1/(4*V4) Tr[M^{-1}dM/dmu M^{-1}dM/dmu]   
        self.iDownImQNSuscConn2    = 27 # = 0                                           
        # strange                                                        
        self.iStrangeReChCond      = 28 #       1/(4*V4) Tr[M^{-1}]                     
        self.iStrangeImChCond      = 29 # = 0                                           
        self.iStrangeReN           = 30 # = 0   1/(4*V4) Tr[dM/dmu M^{-1}]              
        self.iStrangeImN           = 31                                                 
        self.iStrangeReMag         = 32                              
        self.iStrangeUImag         = 33                              
        self.iStrangeReChSuscConn  = 34 #      -1/(4*V4) Tr[M^{-2}]                     
        self.iStrangeImChSuscConn  = 35 # = 0                                           
        self.iStrangeReQNSuscConn1 = 36 #       1/(4*V4) Tr[M^{-1}d2M/dmu2]             
        self.iStrangeImQNSuscConn1 = 37 # = 0                                           
        self.iStrangeReQNSuscConn2 = 38 #      -1/(4*V4) Tr[M^{-1}dM/dmu M^{-1}dM/dmu]  
        self.iStrangeImQNSuscConn2 = 39 # = 0                                           



    def setIndices20Deg2(self):
        # This is expected to work around commit  778a45832097147f4815879021c825c9464dcf1a
        # of YALQCDSS.
        # NOTE: THIS IS EXPECTED TO WORK IN THE CASE WHEN THE LIGHT QUARKS ARE TAKEN
        #       DEGENERATE, AND THERE IS A SINGLE LIGHT QUARK WITH DEGENERATION 2
        
        # The following are     (Degeneracy/(4 V_4))   Tr< something >
        # up
        self.iUpReChCond           =  2 #     1/(4*V4) Tr[M^{-1}]
        self.iUpImChCond           =  3 # = 0 
        self.iUpReN                =  4 # = 0 1/(4*V4) Tr[dM/dmu M^{-1}]
        self.iUpImN                =  5
        self.iUpReMag              =  6                         
        self.iUpUImag              =  7                         
        self.iUpReChSuscConn       =  8 #     1/(4*V4) Tr[M^{-2}]      
        self.iUpImChSuscConn       =  9 # = 0                                           
        self.iUpReQNSuscConn1      = 10 #     1/(4*V4) Tr[M^{-1}d2M/dmu2]               
        self.iUpImQNSuscConn1      = 10 # = 0                                           
        self.iUpReQNSuscConn2      = 12 #     1/(4*V4) Tr[M^{-1}dM/dmu M^{-1}dM/dmu]    
        self.iUpImQNSuscConn2      = 13 # = 0                                           
        # down                                           
        self.iDownReChCond         =  2 #      1/(4*V4) Tr[M^{-1}]                      
        self.iDownImChCond         =  3 # = 0                                           
        self.iDownReN              =  4 # = 0  1/(4*V4) Tr[dM/dmu M^{-1}]               
        self.iDownImN              =  5                                                 
        self.iDownReMag            =  6                           
        self.iDownUImag            =  7                           
        self.iDownReChSuscConn     =  8 #     -1/(4*V4) Tr[M^{-2}]                      
        self.iDownImChSuscConn     =  9 # = 0                                           
        self.iDownReQNSuscConn1    = 10 #      1/(4*V4) Tr[M^{-1}d2M/dmu2]              
        self.iDownImQNSuscConn1    = 10 # = 0                                           
        self.iDownReQNSuscConn2    = 12 #     -1/(4*V4) Tr[M^{-1}dM/dmu M^{-1}dM/dmu]   
        self.iDownImQNSuscConn2    = 13 # = 0                                           
        # strange                                                        
        self.iStrangeReChCond      = 14 #       1/(4*V4) Tr[M^{-1}]                     
        self.iStrangeImChCond      = 15 # = 0                                           
        self.iStrangeReN           = 16 # = 0   1/(4*V4) Tr[dM/dmu M^{-1}]              
        self.iStrangeImN           = 17                                                 
        self.iStrangeReMag         = 18                              
        self.iStrangeUImag         = 19                              
        self.iStrangeReChSuscConn  = 20 #      -1/(4*V4) Tr[M^{-2}]                     
        self.iStrangeImChSuscConn  = 21 # = 0                                           
        self.iStrangeReQNSuscConn1 = 22 #       1/(4*V4) Tr[M^{-1}d2M/dmu2]             
        self.iStrangeImQNSuscConn1 = 23 # = 0                                           
        self.iStrangeReQNSuscConn2 = 24 #      -1/(4*V4) Tr[M^{-1}dM/dmu M^{-1}dM/dmu]  
        self.iStrangeImQNSuscConn2 = 25 # = 0                                           
    
    
    def setIndices20(self):
        # This is expected to work around commit  778a45832097147f4815879021c825c9464dcf1a
        # of YALQCDSS.
        
        # The following are     (Degeneracy/(4 V_4))   Tr< something >
        # up
        self.iUpReChCond           =  2 #     1/(4*V4) Tr[M^{-1}]
        self.iUpImChCond           =  3 # = 0 
        self.iUpReN                =  4 # = 0 1/(4*V4) Tr[dM/dmu M^{-1}]
        self.iUpImN                =  5
        self.iUpReMag              =  6                         
        self.iUpUImag              =  7                         
        self.iUpReChSuscConn       =  8 #     1/(4*V4) Tr[M^{-2}]      
        self.iUpImChSuscConn       =  9 # = 0                                           
        self.iUpReQNSuscConn1      = 10 #     1/(4*V4) Tr[M^{-1}d2M/dmu2]               
        self.iUpImQNSuscConn1      = 10 # = 0                                           
        self.iUpReQNSuscConn2      = 12 #     1/(4*V4) Tr[M^{-1}dM/dmu M^{-1}dM/dmu]    
        self.iUpImQNSuscConn2      = 13 # = 0                                           
        # down                                           
        self.iDownReChCond         = 14 #      1/(4*V4) Tr[M^{-1}]                      
        self.iDownImChCond         = 15 # = 0                                           
        self.iDownReN              = 16 # = 0  1/(4*V4) Tr[dM/dmu M^{-1}]               
        self.iDownImN              = 17                                                 
        self.iDownReMag            = 18                           
        self.iDownUImag            = 19                           
        self.iDownReChSuscConn     = 20 #     -1/(4*V4) Tr[M^{-2}]                      
        self.iDownImChSuscConn     = 21 # = 0                                           
        self.iDownReQNSuscConn1    = 22 #      1/(4*V4) Tr[M^{-1}d2M/dmu2]              
        self.iDownImQNSuscConn1    = 23 # = 0                                           
        self.iDownReQNSuscConn2    = 24 #     -1/(4*V4) Tr[M^{-1}dM/dmu M^{-1}dM/dmu]   
        self.iDownImQNSuscConn2    = 25 # = 0                                           
        # strange                                                        
        self.iStrangeReChCond      = 26 #       1/(4*V4) Tr[M^{-1}]                     
        self.iStrangeImChCond      = 27 # = 0                                           
        self.iStrangeReN           = 28 # = 0   1/(4*V4) Tr[dM/dmu M^{-1}]              
        self.iStrangeImN           = 29                                                 
        self.iStrangeReMag         = 30                              
        self.iStrangeUImag         = 31                              
        self.iStrangeReChSuscConn  = 32 #      -1/(4*V4) Tr[M^{-2}]                     
        self.iStrangeImChSuscConn  = 33 # = 0                                           
        self.iStrangeReQNSuscConn1 = 34 #       1/(4*V4) Tr[M^{-1}d2M/dmu2]             
        self.iStrangeImQNSuscConn1 = 35 # = 0                                           
        self.iStrangeReQNSuscConn2 = 36 #      -1/(4*V4) Tr[M^{-1}dM/dmu M^{-1}dM/dmu]  
        self.iStrangeImQNSuscConn2 = 37 # = 0                                           
    
    
    
    def setIndices10(self):
       
       # This is expected to work around commit 3c65d2ca3530693d941891807a7c1702e7c47185 
       # of YALQCDSS.
       # The following are     (Degeneracy/(4 V_4))   Tr< something >
       # up
       self.iUpReChCond           =  2 #     1/(4*V4) Tr[M^{-1}]
       self.iUpImChCond           =  3 # = 0 
       self.iUpReN                =  4 # = 0 1/(4*V4) Tr[dM/dmu M^{-1}]
       self.iUpImN                =  5 
       self.iUpReChSuscConn       =  6 #     1/(4*V4) Tr[M^{-2}] 
       self.iUpImChSuscConn       =  7 # = 0 
       self.iUpReQNSuscConn1      =  8 #     1/(4*V4) Tr[M^{-1}d2M/dmu2] 
       self.iUpImQNSuscConn1      =  9 # = 0  
       self.iUpReQNSuscConn2      = 10 #     1/(4*V4) Tr[M^{-1}dM/dmu M^{-1}dM/dmu]    
       self.iUpImQNSuscConn2      = 11 # = 0
       # down
       self.iDownReChCond         = 12 #      1/(4*V4) Tr[M^{-1}]                   
       self.iDownImChCond         = 13 # = 0                                     
       self.iDownReN              = 14 # = 0  1/(4*V4) Tr[dM/dmu M^{-1}]              
       self.iDownImN              = 15                                              
       self.iDownReChSuscConn     = 16 #     -1/(4*V4) Tr[M^{-2}]                     
       self.iDownImChSuscConn     = 17 # = 0                    
       self.iDownReQNSuscConn1    = 18 #      1/(4*V4) Tr[M^{-1}d2M/dmu2]
       self.iDownImQNSuscConn1    = 19 # = 0                                        
       self.iDownReQNSuscConn2    = 20 #     -1/(4*V4) Tr[M^{-1}dM/dmu M^{-1}dM/dmu]
       self.iDownImQNSuscConn2    = 21 # = 0         
       # strange             
       self.iStrangeReChCond      = 22 #       1/(4*V4) Tr[M^{-1}]    
       self.iStrangeImChCond      = 23 # = 0                                         
       self.iStrangeReN           = 24 # = 0   1/(4*V4) Tr[dM/dmu M^{-1}]           
       self.iStrangeImN           = 25                                               
       self.iStrangeReChSuscConn  = 26 #      -1/(4*V4) Tr[M^{-2}]                  
       self.iStrangeImChSuscConn  = 27 # = 0                                    
       self.iStrangeReQNSuscConn1 = 28 #       1/(4*V4) Tr[M^{-1}d2M/dmu2]      
       self.iStrangeImQNSuscConn1 = 29 # = 0                        
       self.iStrangeReQNSuscConn2 = 30 #      -1/(4*V4) Tr[M^{-1}dM/dmu M^{-1}dM/dmu]
       self.iStrangeImQNSuscConn2 = 31 # = 0                 
       
       # these quantities are not calculated in old version of YALQCDSS
       self.iStrangeReMag         = -1
       self.iStrangeImMag         = -1
       self.iDownReMag            = -1
       self.iDownImMag            = -1
       self.iUpReMag              = -1
       self.iUpImMag              = -1


