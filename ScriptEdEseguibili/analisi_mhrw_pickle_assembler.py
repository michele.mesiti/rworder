#!/usr/bin/env python3

import numpy as np
from sys import argv,stdout,stderr,exit
import pickle
import osservabili_rw_mhrw as orw



def join_pickles(allFileNames):
    allObsValuesPerBetaJackKnifes = [] # the original data. 
                                       # will contain all jk resamples
    betas = None
    obsNames = None

    for fileName in allFileNames:
        stdout.write("Reading {} \n".format(fileName))
    
        f = open(fileName,'rb')
        tempPickle = pickle.load(f)
        newBetas = list(tempPickle[0].keys())
        newObsNames = list(tempPickle[0][newBetas[0]].keys())

        newBetas.sort()
        newObsNames.sort()
        if betas is not None and obsNames is not None:
            if betas != newBetas  :
                raise Exception('Betas from '+ fileName +' are not compatible')
            if obsNames != newObsNames :
                print(obsNames)
                print(newObsNames)
                raise Exception('obsNames from '+ fileName +
                        ' are not compatible')
        else :
            betas = newBetas
            obsNames = newObsNames 

        allObsValuesPerBetaJackKnifes += tempPickle
        f.close()

    stdout.write("Joined all chunks.\n")

    betas.sort()

    return allObsValuesPerBetaJackKnifes, obsNames, betas

def transpose_pickle(joinedPickle,obsNames,betas):
    '''The format of each pickle file is a
        list [no_jk_resamples_in_chunk] of
          dicts [all betas] of
             dicts [all obs names ]
                (a single float value per observable).
        We need to assemble all pickle files, and then
        we need to rearrange the data structure.
    '''
    allJackKnifesBetaPerObs = dict() # the returned object 
    
    for obsName in obsNames:

        obsOfBeta = []

        for beta in betas:
             obsOfBeta.append(np.array( [element[beta][obsName] for element in\
                     joinedPickle ]))

        allJackKnifesBetaPerObs[obsName] = np.array(obsOfBeta)

    # dict(np.array(nbetas x njk))
    return allJackKnifesBetaPerObs, betas


def calc_mean_err_obs(allJackKnifesBetaPerObs,betas):

    meanErrs = dict()

    for obsName,item in allJackKnifesBetaPerObs.items():
        means = np.mean(item,axis = 1)
        stds = np.std(item,axis = 1,ddof=1) * np.sqrt(item.shape[1])
        meanErrs[obsName] = np.array(betas), means, stds

    return meanErrs


def xmax_from3points(x,y):

    x = np.array(x)
    y = np.array(y).reshape(3,1)

    M = np.matrix(np.vstack([x**n for n in range(3)]).T)
      

    c,b,a = np.array(M.I * y).flatten() #parameters

    return -b/(2*a)


def calc_maxima_mean_err_purejk(allJackKnifesBetaPerObs,betas):

    meanErrs = dict()
    betas = np.array(betas)

    for obsName,item in allJackKnifesBetaPerObs.items():
        imaxes = np.argmax(item,axis = 0) # [njk]
        betas_max = [] # will be [njk]
        ilim = item.shape[0] # no of beta values
        assert item.shape[0] == len(betas)

        for jk,imax in enumerate(imaxes):
            if imax == 0:
                betas_max.append(betas[0])
            elif imax == ilim-1:
                betas_max.append(betas[-1])
            else:
                #points close to the peak
                ty = item[imax-1:imax+2,jk]
                tx = betas[imax-1:imax+2]
                betas_max.append(xmax_from3points(tx,ty))

        mean = np.mean(betas_max)
        std = np.std(betas_max,ddof=1) * np.sqrt(item.shape[1]) 
        meanErrs[obsName] = mean,std
    return meanErrs


def calc_maxima_mean_err_intercept(meanErrs_xydy):

    meanErrs = dict()
    for obsName, item in meanErrs_xydy.items():
        betas,y,dy = item
        yintercept = np.max(y)
        condition = y+dy > yintercept
        arglimp = np.max(np.arange(len(betas))[condition])
        if arglimp < len(betas)-1:
            betapm,ypm = betas[arglimp]  ,(y+dy)[arglimp]
            betapp,ypp = betas[arglimp+1],(y+dy)[arglimp+1]
            dyp = ypp-ypm # < 0
    
            betap = (betapm * (ypp-yintercept) - betapp * (ypm-yintercept))/dyp
        else:
            betap = betas[arglimp]

        arglimm = np.min(np.arange(len(betas))[condition])
        if arglimm > 0:
            betamm,ymm = betas[arglimm-1],(y+dy)[arglimm-1]
            betamp,ymp = betas[arglimm],  (y+dy)[arglimm]
            dym = ymp-ymm # > 0
    
            betam = (betamm * (ymp-yintercept) - betamp * (ymm-yintercept))/dym
        else:
            betam = betas[arglimm]

        betamax = (betap+betam)/2
        betaerr = (betap-betam)/2
        meanErrs[obsName] = betamax,betaerr
    return meanErrs


def doall(mode,rootName,allFileNames):

    if mode == 'gauge':
        obsFuns = orw.gaugeObsMHRW
        funNames = [ obsFun.__name__ for obsFun in orw.gaugeObsMHRW ] 
    if mode == 'fermion':
        obsFuns = orw.fermObsMHRW
        funNames = [ obsFun.__name__ for obsFun in orw.fermObsMHRW ] 


    valuesLists = dict()
    totBlocks = 0
    



    allObsValuesPerBetaJackKnifes, obsNames, betaPoints =\
            join_pickles(allFileNames)


    # creating lists
    
    # loading data, concatenating all the lists
    #for fileName in allFileNames:
    #
    #    stdout.write("Reading {} \n".format(fileName))
    #
    #    f = open(fileName,'rb')
    #    allObsValuesPerBetaJackKnifes = pickle.load(f)
    #    f.close()
    #
    #    for obsFun in obsFuns:
    #        #print( obsFun.__name__)
    #        if obsFun.__name__ not in valuesLists:
    #            valuesLists[obsFun.__name__] = dict() # [nBetas]
    #        if betaPoints is None:
    #            betaPoints = allObsValuesPerBetaJackKnifes[0].keys()
    #            #print(allObsValuesPerBetaJackKnifes[0].keys())
    #
    #        for betaPoint in betaPoints:
    #            if betaPoint not in valuesLists[obsFun.__name__]:
    #                valuesLists[obsFun.__name__][betaPoint] = []
    #            valueList =  [ element[betaPoint][obsFun.__name__] for element in\
    #                     allObsValuesPerBetaJackKnifes]
    #            valuesLists[obsFun.__name__][betaPoint] += valueList
    #
    
    allJackKnifesBetaPerObs, betaPoints = \
            transpose_pickle(allObsValuesPerBetaJackKnifes, 
                    obsNames, betaPoints)

    meanErrObs = calc_mean_err_obs(allJackKnifesBetaPerObs, betaPoints)

  
   # funMeanErrJackKnife = dict()
   # for obsFun in obsFuns:
   #     funMeanErrJackKnife[obsFun.__name__] = dict() # [nBetas]
   #     for betaPoint in betaPoints:
   #         totBlocksCheck = len(valuesLists[obsFun.__name__][betaPoint])
   #         valuesLists[obsFun.__name__][betaPoint] = np.array(valuesLists[obsFun.__name__][betaPoint])
   #         if totBlocks == 0:
   #             totBlocks = totBlocksCheck
   #         #elif totBlocks != totBlocksCheck:
   #             #stdout.write("Error: totBlocksCheck {} vs totBlocks {} ( observable:{} beta:{})\n"\
   #             #        .format(totBlocksCheck, totBlocks, obsFun.__name__, betaPoint ))
   #         #stdout.write("TotBlocksCheck {} vs totBlocks {} ( observable:{} beta:{})\n"\
   #         #        .format(totBlocksCheck, totBlocks, obsFun.__name__, betaPoint ))
   # 
   # 
   #         funMeanErrJackKnife[obsFun.__name__][betaPoint] = \
   #                     np.mean(valuesLists[obsFun.__name__][betaPoint]),\
   #                     np.std(valuesLists[obsFun.__name__][betaPoint], ddof=1)\
   #                     *np.sqrt(totBlocks) # YOU KNOW WHY
   # 
    
    
    
    if mode == 'gauge':
        outfileName = rootName + '.MHRWout.gauge.dat'
    if mode == 'fermion':
        outfileName = rootName + '.MHRWout.fermion.dat'
    
    
    header = 'beta'
    for funName in funNames:
        header += "\t{}\t(err)".format(funName)
    header += '\n'

   
    betaPoints = np.array(betaPoints)
    
    meanErrObsList = [ meanErrObs[funName] for funName in funNames ]
    arrsToStack = [betaPoints] + [ np.vstack((el[1],el[2])) 
                                                      for el in meanErrObsList] 

    stackedArrs = np.vstack(arrsToStack).T

    stdout.write("Writing file {}\n".format(outfileName))
    np.savetxt(outfileName,stackedArrs,header=header)

  
    maximaMeanErrsPureJK = \
            calc_maxima_mean_err_purejk(allJackKnifesBetaPerObs, betaPoints)

    maximaMeanErrsIntercept = \
            calc_maxima_mean_err_intercept(meanErrObs)

    maximaFileName = 'maxima_' + mode + '_' + rootName 
    print("Writing {}".format(maximaFileName))
    maximaFile = open(maximaFileName,'w')
 
    maximaFile.write('#obsName, maxpurejk, err, maxintercept, err\n')

    for funName in [ fn for fn in funNames if 'susc' in fn.lower() ]:
    
        meanMaxPJK,stdMaxPJK =  maximaMeanErrsPureJK[funName]
        meanMaxI,stdMaxI =  maximaMeanErrsIntercept[funName]
        maximaFile.write("{}\t".format(funName))
        maximaFile.write("{}\t{}\t".format(meanMaxPJK,stdMaxPJK))
        maximaFile.write("{}\t{}\n".format(meanMaxI,stdMaxI))
    
    maximaFile.close()
        
    


####################################

if __name__ == '__main__':
    mode = ''
    if 'gauge' in argv and 'fermion' not in argv:
        mode = 'gauge'
        argv.remove('gauge')
        obsFuns = orw.gaugeObsMHRW
        funNames = [ obsFun.__name__ for obsFun in orw.gaugeObsMHRW ] 
    elif 'fermion' in argv and 'gauge' not in argv:
        mode = 'fermion'
        argv.remove('fermion')
        obsFuns = orw.fermObsMHRW
        funNames = [ obsFun.__name__ for obsFun in orw.fermObsMHRW ] 
    elif 'fermion' in argv and 'gauge' in argv:
        print("EITHER gauge OR fermion")
        exit(1)
    else:
        print("'gauge' or 'fermion' must be present")
        exit(1)
    
   
    rootName = argv[1]
    allFileNames = argv[2:]
    doall(mode,rootName,allFileNames)
    

