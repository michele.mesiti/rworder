#!/bin/bash


RIGHT_CONF_FILE='run.mass0.00*_B3.*.conf'
#RIGHT_CONF_FILE='run.conf.right_one'

echo -e '#'beta"\t"therm"\t"block"\t"nCopies"\t"logZetasMHRWstart"\t"runId"\t"volume"\t"LASTGAUGEFILE "\t"LASTFERMFILE"\t"MODE"\t"CUTMODE
for dir in $@
do
    # FINDING THE LAST GAUGE AND FERMION FILES AS THE ONE WHICH REACH THE HIGHEST NTRAJ
    LASTGAUGEFILE='NOTFOUND'
    LASTGAUGETRAJ=0
    for gaugefile in $dir/gauge*
    do
        TEMPTRAJ=$(tail -n 1 $gaugefile | awk '{print $1}') 
        if [ "$TEMPTRAJ" -gt "$LASTGAUGETRAJ" ]
        then
            LASTGAUGETRAJ=$TEMPTRAJ
            LASTGAUGEFILE=$gaugefile
        fi
    done
    LASTFERMFILE='NOTFOUND'
    LASTFERMTRAJ=0
    for fermfile in $dir/ferm*
    do
        TEMPTRAJ=$(tail -n 1 $fermfile | awk '{print $1}') 
        if [ "$TEMPTRAJ" -gt "$LASTFERMTRAJ" ]
        then
            LASTFERMTRAJ=$TEMPTRAJ
            LASTFERMFILE=$fermfile
        fi
    done
    beta=$(grep -i beta $dir/$RIGHT_CONF_FILE| awk '{print $2}')
    nx=$(grep ^nx $dir/$RIGHT_CONF_FILE| awk '{print $2}')
    ny=$(grep ^ny $dir/$RIGHT_CONF_FILE| awk '{print $2}')
    nz=$(grep ^nz $dir/$RIGHT_CONF_FILE| awk '{print $2}')
    nt=$(grep ^nt $dir/$RIGHT_CONF_FILE| awk '{print $2}')
    therm=500
    block=30
    nFermCopies=$(head -100 $LASTFERMFILE | sort -k2 -n | awk '{print $2}'| tail -n1)
    nFermCopies=$((nFermCopies+1))
    logZetasMHRWstart=0
    runId=1
    volume=$(($nx*$ny*$nz*$nt))
    noOfFields=$(tail -n 1 $LASTFERMFILE | awk '{print NF}')
    if [ "$noOfFields" -eq "32" ]
    then 
        MODE='mode1.0'
    elif [ "$noOfFields" -eq "38" ]
    then 
        MODE='mode2.0'
    elif  [ "$noOfFields" -eq "26" ]
    then
        MODE='mode2.0Deg2'
    else
        MODE="MODE_NOT_RECOGNIZED!!"
    fi

    # in order to check if something went wrong - like, some measurement was skipped
    awk '(NR>1){print $1}' $LASTFERMFILE | uniq -c | grep -Ev '\s*'$nFermCopies | awk '{print $2}'

    CUTMODE='ntrajBased'

    echo -e  $beta"\t"$therm"\t"$block"\t"$nFermCopies"\t"$logZetasMHRWstart"\t\t\t"$runId"\t"$volume"\t"$LASTGAUGEFILE"\t"$LASTFERMFILE"\t"$MODE"\t"$CUTMODE


done 
