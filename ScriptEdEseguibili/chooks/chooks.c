#include <stdio.h>
#include <math.h>


// this function does the following thing:
// 1.  x1,x2,..,xn
// 2.  e^x1,e^x2,..,e^xn  // this must not be performed explicitly for numerical issues
// 3.  sum all
// 4. take the log
//
// This is not 


inline double logSumFromLog(double a,double b){

    if(a>b) return a + log1p(exp(b-a));
    else return b + log1p(exp(a-b));

}

double logSumFromLogArr(const double *arr, int size)
{

    int i; 
    double res;
    if(size==1) res = arr[0];
    else{
        res =logSumFromLog(logSumFromLogArr(arr,(int)size/2),
                logSumFromLogArr(&arr[(int)size/2],size - (int)size/2));
    }
    return res;

}

// sums only on the 2nd dimension 
void logSumFromLog2DArr(const double *arr,// [nRows,nCols]
        int nRows, int nCols,
        double *outs)// [nRows]
{

    int iRow;
    for(iRow = 0; iRow < nRows ; iRow++ ){
        int offset = iRow * nCols;
        outs[iRow] = logSumFromLogArr(&arr[offset],nCols);
    }
}



// it is assumed that the array is C-ordered, [nRow,nCol]
// sum of the N*N-1 products of different 

void lineSumNNmProd(const double *arr2D, int nRow, int nCol,double *arr1Dout)
{
    int iRow;
    for(iRow=0;iRow<nRow;iRow++){
        arr1Dout[iRow]= 0;
        int rowOffset = iRow*nCol;
        int iCol1,iCol2;
        int i;
        for(iCol1=0;iCol1<nCol-1;iCol1++) for(iCol2=iCol1+1;iCol2<nCol;iCol2++){
            arr1Dout[iRow] += arr2D[rowOffset+iCol1]*arr2D[rowOffset+iCol2];
        }
    }
}

