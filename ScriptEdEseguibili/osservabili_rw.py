#!/usr/bin/env python3
# This python library is provided to analyse the data produced by YALQCDSS.
# This is expected to work around commit 3c65d2ca3530693d941891807a7c1702e7c47185 
# of YALQCDSS.

import numpy as np
from bootstrapblocking.jackknife_blocking import * # deprecated, but who gives a fuck
from sys import stderr

from all_indices import *

# Fermionic composed observables

verbose = False

# To calculate pieces like   (Degeneracy/(4 V_4))^2   < ( Tr[something] )^2 >
# That is
#    1. The first part of the disconnected chiral susceptibility
#    2. The first part of the disconnected quark number susceptibility
# Notice that you will have to multiply for the right factor afterward (a V4, likely).
# This function must be inserted in a jackknife-using function 
# (See, for example the suscepbility and binder cumulant functions in the
# included library )

def discPieceType1(data,args): # data should be 2D [nMeas,nCopies*nObs]
    # returns 1D array  [nMeas]
    nCopies, indices = args
    # indices is a list of indices which can relate to the strange quark o
    nMeas = data.shape[0]
    nObs = int(np.floor(data.shape[1]/nCopies))
    indices = list(set(indices))   # taking the unique elements
    nChosen = len(indices)

    #reshaping to a [nMeas,nCopies,nObs] array
    dataReshaped = np.reshape(data,(nMeas,nCopies,nObs))#copies on column

    # selecting the important indices, recasting to [nmeas,nCopies*nChosen] shape
    dataReshaped = np.reshape(dataReshaped[:,:,indices],(nMeas,nCopies*nChosen))

    
    # CLEVER METHOD
    # summing all terms and taking the square    
    sumOfAllProducts = np.sum(dataReshaped,axis=1)**2  #[nMeas]
    # calculating the contact terms to remove
    contactTerms = np.sum(dataReshaped**2,axis=1)       #[nMeas]
 
    return (sumOfAllProducts - contactTerms)/(nCopies*nChosen*(nCopies*nChosen-1))# [nMeas]




def ChiralSuscLight(data,blockSize,nCopies, V4,idx): # data is [nMeas*nCopies,nObs]

    nMeas = int(np.floor(data.shape[0] / nCopies))
    nObs  = data.shape[1] 
    nBlocks = int(np.floor(nMeas/blockSize))
    nMeasCut = nBlocks*blockSize

    if verbose:
        stderr.write("nMeas {}, nObs {}, nBlocks {}, nMeasCut {}\n".format(nMeas,nObs,nBlocks,nMeasCut))

    args = (nCopies,(idx.iUpReChCond,idx.iDownReChCond))
    dataReshaped = np.reshape(data[:nMeasCut*nCopies,:],(nMeasCut,nCopies*nObs))
    if idx.iUpReChCond == idx.iDownReChCond:
        degeneracy = 2
    else :
        degeneracy = 1

    # N-1 blocks
    # NOTATION : '...sNm' stands for:
    # s : is plural, it's an array nBlocks long
    # Nm : N-1, N minus 1, it's the observable calculated on N-1 blocks (which is biased)

    # 1
    # averaging over light quarks
    lightDisc1sNm = V4*jackknifeBlockingLocalArray(dataReshaped,\
            blockSize,discPieceType1,args)

    # 2
    noOfProductsPerQuark = blockSize*(nBlocks-1)*nCopies
    upDisc2sNm = jackknifeBlockingLocalArrayPower(data[:nMeasCut*nCopies,:],\
            blockSize*nCopies,1,idx.iUpReChCond) 
    downDisc2sNm = jackknifeBlockingLocalArrayPower(data[:nMeasCut*nCopies,:],\
            blockSize*nCopies,1,idx.iDownReChCond)
    # we want to take the square, but we need to get rid of contact terms
    # note: the contact terms here will count only  as 1/(total numer of noise vectors)
    # so the effect of this will be largely negligible

    upDisc2ContactsNm = jackknifeBlockingLocalArrayPower(data[:nMeasCut*nCopies,:],\
            blockSize*nCopies,2,idx.iUpReChCond) 
    downDisc2ContactsNm = jackknifeBlockingLocalArrayPower(data[:nMeasCut*nCopies,:],\
            blockSize*nCopies,2,idx.iDownReChCond)

    # averaging over light quarks
    lightDisc2sNm =  ((upDisc2sNm + downDisc2sNm)/2)**2 * noOfProductsPerQuark - \
            (upDisc2ContactsNm+downDisc2ContactsNm)/2
    lightDisc2sNm /= (noOfProductsPerQuark-1)

    # notice : a V4 factor is missing, so 
    lightDisc2sNm *=  - V4

    # 3
    upConnsNm = jackknifeBlockingLocalArrayPower(data[:nMeasCut*nCopies,:],\
            blockSize*nCopies,1,idx.iUpReChSuscConn)
    downConnsNm = jackknifeBlockingLocalArrayPower(data[:nMeasCut*nCopies,:],\
            blockSize*nCopies,1,idx.iDownReChSuscConn)
    # averaging over light quarks
    lightConnsNm = (upConnsNm + downConnsNm)/2

    # mean over the nBlocks jackknife samples
    # NOTE: if degeneracy == 2, lightDisc1sNm and lightDisc2sNm are 4 times the 
    #       single quark value, but lightConnsNm is just 2 times.
    #       (data in files is multiplied by degeneracy) 
    chSuscLightNm = np.mean(lightDisc1sNm/degeneracy**2 + 
                               lightDisc2sNm/degeneracy**2 + 
                               lightConnsNm/degeneracy)
 

    # N blocks
    # using data[:nMeasCut*nCopies,:], the leftovers cannot be used.
    # 1
    lightDisc1N = V4 * np.mean(discPieceType1(dataReshaped,args))

    # 2
    noOfProductsPerQuark = blockSize*nBlocks*nCopies
    upDisc2N = np.mean(data[:nMeasCut*nCopies,idx.iUpReChCond])
    downDisc2N = np.mean(data[:nMeasCut*nCopies,idx.iDownReChCond])

    # we want to take the square, but we need to get rid of contact terms
    # see consideration above on the negligibility of contact terms in this case

    upDisc2ContactN = np.mean(data[:nMeasCut*nCopies,idx.iUpReChCond]**2)
    downDisc2ContactN = np.mean(data[:nMeasCut*nCopies,idx.iDownReChCond]**2)

    # averaging over light quarks
    lightDisc2N = ((upDisc2N + downDisc2N)/2)**2 * noOfProductsPerQuark - \
            (upDisc2ContactN + downDisc2ContactN)/2
    lightDisc2N /= (noOfProductsPerQuark-1)


    # notice : a V4 factor is missing, so 
    lightDisc2N *= -V4
 
    # 3 
    upConnN = np.mean(data[:nMeasCut*nCopies,idx.iUpReChSuscConn])
    downConnN = np.mean(data[:nMeasCut*nCopies,idx.iDownReChSuscConn])
    lightConnN = (upConnN + downConnN)/2

    # NOTE: if degeneracy == 2, lightDisc1N and lightDisc2N are 4 times the 
    #       single quark value, but lightConnN is just 2 times.
    #       (data in files is multiplied by degeneracy) 
    chSuscLightN = (lightDisc1N/degeneracy**2 + 
                    lightDisc2N/degeneracy**2 + 
                    lightConnN /degeneracy )

    if verbose :
        stderr.write( "lightDisc1Nm {},  lightDisc2Nm {}, lightConnNm {}\n".format(np.mean(lightDisc1sNm), np.mean(lightDisc2sNm) , np.mean(lightConnsNm)))
        stderr.write("chSuscLightNm {}\n".format(chSuscLightNm))
    
        stderr.write( "lightDisc1N {},  lightDisc2N {}, lightConnN {}\n".format(lightDisc1N, lightDisc2N , lightConnN)) 
        stderr.write("chSuscLightN {}\n".format(chSuscLightN))
    
        stderr.write("Bias Analysis:\n")
        stderr.write("delta disc 1    {}\n".format(lightDisc1N-np.mean(lightDisc1sNm))) 
        stderr.write("delta disc 2    {}\n".format(lightDisc2N-np.mean(lightDisc2sNm)))
        stderr.write("delta disc conn {}\n".format(lightConnN -np.mean(lightConnsNm)))

    #unbiased value
    chSuscLight = nBlocks * chSuscLightN - (nBlocks-1) * chSuscLightNm

    # jackknife error estimate (not bias corrected)
    # Notice: the sqrt(nBlocks) factor is necessary because 
    # the stddev of the jackknife sample is 1/nBlocks \sigma  ( the stddev of the variable):
    # [the jaccknife quantities are indeed closer to the SAMPLE mean (not the real one)
    # than the original data by a factor 1/nBlock], but the standard error is 1/sqrt(nBlocks \sigma).

    chSuscLightErrJNm = np.sqrt(nBlocks)*np.std(lightDisc1sNm + lightDisc2sNm + lightConnsNm, ddof = 1)

    return chSuscLight, chSuscLightErrJNm


def imLightN2(data,blockSize,nCopies,V4,idx):  # data is [nMeas*nCopies,nObs]

    nMeas = int(np.floor(data.shape[0] / nCopies))
    nObs  = data.shape[1] 
    nBlocks = int(np.floor(nMeas/blockSize))
    nMeasCut = nBlocks*blockSize

    args = (nCopies,(idx.iUpImN,idx.iDownImN)) # we'll take the IMAGINARY part 

    if idx.iUpReChCond == idx.iDownReChCond:
        degeneracy = 2
    else :
        degeneracy = 1

    dataReshaped = np.reshape(data[:nMeasCut*nCopies,:],(nMeasCut,nCopies*nObs))

    # N-1 blocks
    # NOTATION : '...sNm' stands for:
    # s : is plural, it's an array nBlocks long
    # Nm : N-1, it's the observable calculated on

    # 1
    # averaging over light quarks
    lightDisc1sNm = V4*jackknifeBlockingLocalArray(dataReshaped,\
            blockSize,discPieceType1,args)
    # NOTE: if degeneracy == 2, is lightDisc1sNm 4 times the 
    #       single quark value
    #       (data in files is multiplied by degeneracy) 
    QN2LightNm = np.mean(lightDisc1sNm ) / degeneracy**2

    
    # N blocks
    # using data[:nMeasCut*nCopies,:], the leftovers cannot be used.
    # 1
    lightDisc1N = V4 * np.mean(discPieceType1(dataReshaped,args))

    QN2LightN = lightDisc1N / degeneracy**2
    

    # unbiased value
    QN2Light = nBlocks * QN2LightN - (nBlocks-1) * QN2LightNm


    # jackknife error estimate (not bias corrected)
    # Notice: the sqrt(nBlocks) factor is necessary, see comments above 
    QN2ErrJNm = np.sqrt(nBlocks)*np.std(lightDisc1sNm,ddof = 1)
  
    if True:
        stderr.write("nCopies {}\n".format(nCopies ))
        stderr.write("nMeas   {}\n".format(nMeas   ))
        stderr.write("nObs    {}\n".format(nObs    ))
        stderr.write("nBlocks {}\n".format(nBlocks ))
        stderr.write("nMeasCut{}\n".format(nMeasCut))
        stderr.write("Bias Analysis:\n")
        stderr.write("QN2LightN: {}\n".format(QN2LightN))
        stderr.write("QN2LightNm: {}\n".format(QN2LightNm))


    return QN2Light,QN2ErrJNm




def absImLightNDiscSusc(data,blockSize,nCopies,V4,idx):  # data is [nMeas*nCopies,nObs]

    nMeas = int(np.floor(data.shape[0] / nCopies))
    nObs  = data.shape[1] 
    nBlocks = int(np.floor(nMeas/blockSize))
    nMeasCut = nBlocks*blockSize

    args = (nCopies,(idx.iUpImN,idx.iDownImN)) # we'll take the IMAGINARY part 
    if idx.iUpReChCond == idx.iDownReChCond:
        degeneracy = 2
    else :
        degeneracy = 1

    dataReshaped = np.reshape(data[:nMeasCut*nCopies,:],(nMeasCut,nCopies*nObs))

    # N-1 blocks
    # NOTATION : '...sNm' stands for:
    # s : is plural, it's an array nBlocks long
    # Nm : N-1, it's the observable calculated on

    # 1
    # averaging over light quarks
    lightDisc1sNm = V4*jackknifeBlockingLocalArray(dataReshaped,\
            blockSize,discPieceType1,args)

    # 2
    # we have to take the noise means and then take the absolute value
    # averaging over light quarks
    imLightNData = data[:nMeasCut*nCopies,args[1]]
    imLightNDataNoiseMeans = np.mean(imLightNData.reshape(nMeasCut,nCopies*len(args[1])),
            axis = 1 )
                                        # 2 because we have chosen 2 columns and 2 quarks
    absImLightNDataNoiseMeans = np.abs(imLightNDataNoiseMeans).reshape((-1,1))
    #absImLightNDataNoiseMeans = imLightNDataNoiseMeans.reshape((-1,1))
    

    lightDisc2sNm = jackknifeBlockingLocalArrayPower(absImLightNDataNoiseMeans,\
            blockSize,1,0)**2

    # notice : a V4 factor is missing, so 
    lightDisc2sNm *= - V4


    # NOTE: if degeneracy == 2, is lightDisc1sNm 4 times the 
    #       single quark value as lightDisc2sNm
    #       (data in files is multiplied by degeneracy) 

    QNDiscSuscLightNm = np.mean(lightDisc1sNm/degeneracy**2 + 
                                lightDisc2sNm/degeneracy**2 )

    
    # N blocks
    # using data[:nMeasCut*nCopies,:], the leftovers cannot be used.
    # 1
    lightDisc1N = V4 * np.mean(discPieceType1(dataReshaped,args))

    # 2
    # we have to take the noise means and then take the absolute value
    # averaging over light quarks
    lightDisc2N = np.mean(absImLightNDataNoiseMeans) **2


    # notice : a V4 factor is missing, so 
    lightDisc2N *= - V4
 
    QNDiscSuscLightN = (lightDisc1N/degeneracy**2 + 
                        lightDisc2N/degeneracy**2 )
    

    # unbiased value
    QNDiscSuscLight = nBlocks * QNDiscSuscLightN - (nBlocks-1) * QNDiscSuscLightNm



    # jackknife error estimate (not bias corrected)
    # Notice: the sqrt(nBlocks) factor is necessary, see comments above 
    QNDiscSuscLightErrJNm = np.sqrt(nBlocks)*np.std(lightDisc1sNm + lightDisc2sNm ,\
            ddof = 1)
   
    if verbose : 
        stderr.write("Bias Analysis:\n")
        stderr.write("QNDiscSuscLight: {}\n".format(QNDiscSuscLight))
        stderr.write("QNDiscSuscLightN: {} (Disc1: {}, Disc2: {})\n".format(\
                QNDiscSuscLightN,lightDisc1N,lightDisc2N))
        stderr.write("QNDiscSuscLightNm: {} (Disc1: {}, Disc2 : {})\n".format(\
                QNDiscSuscLightNm,np.mean(lightDisc1sNm),np.mean(lightDisc2sNm)))



    return QNDiscSuscLight,QNDiscSuscLightErrJNm 



       


def bareLightCond(data,blockSize,nCopies, V4,idx):#V4 is useless, but keep for uniformity
                                                  # with the other fermionic variables
    # <uu> + <dd>


    nMeas = int(np.floor(data.shape[0] / nCopies))
    nObs  = data.shape[1] 
    nBlocks = int(np.floor(nMeas/blockSize))
    nMeasCut = nBlocks*blockSize
    if idx.iUpReChCond == idx.iDownReChCond:
        degeneracy = 2
    else :
        degeneracy = 1


    if verbose:
        stderr.write("nMeas {}, nObs {}, nBlocks {}, nMeasCut {}\n".format(nMeas,nObs,nBlocks,nMeasCut))
        stderr.write("idx.iUpReChCond: {} , idx.iDownReChCond {}\n".format(idx.iUpReChCond, idx.iDownReChCond))


    # N-1 blocks
    upCondsNm = jackknifeBlockingLocalArrayPower(data[:nMeasCut*nCopies,:],\
            blockSize*nCopies,1,idx.iUpReChCond) 

    downCondsNm = jackknifeBlockingLocalArrayPower(data[:nMeasCut*nCopies,:],\
            blockSize*nCopies,1,idx.iDownReChCond) 

    # NOTE: if degeneracy == 2,downCondsNm is 4 times the 
    #       single quark value as upCondsNm
    #       (data in files is multiplied by degeneracy) 

    lightChCondsNm = (upCondsNm + downCondsNm ) / degeneracy # this is <uu> + <dd>


    # observable is very simple and unbiased.

    lightChCond = np.mean(lightChCondsNm)

    # jackknife error estimate
    # see above for the justification of np.sqrt(nBlocks)

    lightChCondErr = np.sqrt(nBlocks)*np.std(lightChCondsNm)

    return lightChCond, lightChCondErr


def absImQuarkNumberLight(data,blockSize,nCopies,V4,idx):#V4 is useless, for uniformity
                                                  # with the other fermionic variables

    nMeas = int(np.floor(data.shape[0] / nCopies))
    nObs  = data.shape[1] 
    nBlocks = int(np.floor(nMeas/blockSize))
    nMeasCut = nBlocks*blockSize
    if idx.iUpReChCond == idx.iDownReChCond:
        degeneracy = 2
    else :
        degeneracy = 1


    if verbose:
        stderr.write("nMeas {}, nObs {}, nBlocks {}, nMeasCut {}\n".format(nMeas,nObs,nBlocks,nMeasCut))
        stderr.write("idx.iUpImN: {} , idx.iDownImN {}\n".format(idx.iUpImN, idx.iDownImN))

    dataImLightNOnly = data[:nMeasCut*nCopies,[idx.iUpImN,idx.iDownImN]]

    dataReshapedImLightNOnly = np.reshape(dataImLightNOnly, (nMeasCut,nCopies*2))
                                                 # 2  = no of indices chosen
                                                 # UP-DOWN total degeneracy IS ASSUMED!

    imLightNOnlyNoiseMeans = np.mean(dataReshapedImLightNOnly, axis = 1 )

    # NOTE: if degeneracy == 2,imLightNOnlyNoiseMeans is 2 times the 
    #       single quark value 
    #       (data in files is multiplied by degeneracy) 

    imLightNOnlyNoiseMeans /= degeneracy

    absImLightNOnlyNoiseMeans = np.abs(imLightNOnlyNoiseMeans) 

    # N-1 blocks
    absImLightNOnlyNoiseMeans = absImLightNOnlyNoiseMeans.reshape((-1,1))
    
    absImLightsNm =  jackknifeBlockingLocalArrayPower(absImLightNOnlyNoiseMeans,\
            blockSize,1,0) 

    # observable is very simple and unbiased.

    absImLightN = np.mean(absImLightsNm)

    # jackknife error estimate
    # see above for the justification of np.sqrt(nBlocks)

    absImLightNErr = np.sqrt(nBlocks)*np.std(absImLightsNm)

    return absImLightN,  absImLightNErr



def renLightCond1(data,blockSize,nCopies, V4,idx): # V4 is useless, but keep for uniformity
                                               # with the other fermionic variables
     
    # <uu> + <dd> - 2m_l/m_s <ss>

    mass_ratio = 1/28.15

    nMeas = int(np.floor(data.shape[0] / nCopies))
    nObs  = data.shape[1] 
    nBlocks = int(np.floor(nMeas/blockSize))
    nMeasCut = nBlocks*blockSize
    if idx.iUpReChCond == idx.iDownReChCond:
        degeneracy = 2
    else :
        degeneracy = 1


    if verbose:
        stderr.write("nMeas {}, nObs {}, nBlocks {}, nMeasCut {}\n".format(nMeas,nObs,nBlocks,nMeasCut))

    dataReshaped = np.reshape(data[:nMeasCut*nCopies,:],(nMeasCut,nCopies*nObs))


    # N-1 blocks
    upCondsNm = jackknifeBlockingLocalArrayPower(data[:nMeasCut*nCopies,:],\
            blockSize*nCopies,1,idx.iUpReChCond) 

    downCondsNm = jackknifeBlockingLocalArrayPower(data[:nMeasCut*nCopies,:],\
            blockSize*nCopies,1,idx.iDownReChCond) 

    strangeCondsNm = jackknifeBlockingLocalArrayPower(data[:nMeasCut*nCopies,:],\
            blockSize*nCopies,1,idx.iStrangeReChCond) 

    
    # NOTE : if degeneracy == 2, we're counting light quarks twice
    #        (because data is already multiplied by 2 in data files)
    lightRenChCondsNm = (upCondsNm + downCondsNm)/degeneracy - 2* mass_ratio * strangeCondsNm


    # observable is very simple and unbiased.

    lightRenChCond = np.mean(lightRenChCondsNm)

    # jackknife error estimate
    # see above for the justification of np.sqrt(nBlocks)

    lightRenChCondErr = np.sqrt(nBlocks)*np.std(lightRenChCondsNm)

    return lightRenChCond, lightRenChCondErr



def rePolyLoop(data,blockSize,idx):

    nMeas = data.shape[0]
    nObs  = data.shape[1] 
    nBlocks = int(np.floor(nMeas/blockSize))
    nMeasCut = nBlocks*blockSize

    
    # N-1 - see above for suffix meaning
    rePolysNm = jackknifeBlockingLocalArrayPower(data[:nMeasCut,:],\
            blockSize,1,idx.iRePoly);


    rePoly = np.mean(rePolysNm)


    # jackknife error (see above for exlpanation)
    rePolyErrJNm = np.sqrt(nBlocks) * np.std(rePolysNm)

    return rePoly, rePolyErrJNm


def absImPolyLoop(data,blockSize,idx):

    nMeas = data.shape[0]
    nObs  = data.shape[1] 
    nBlocks = int(np.floor(nMeas/blockSize))
    nMeasCut = nBlocks*blockSize

    
    # N-1 - see above for suffix meaning
    imPolysNm = jackknifeBlockingLocalArrayPower(np.abs(data[:nMeasCut,:]),\
            blockSize,1,idx.iImPoly);


    imPoly = np.mean(imPolysNm)


    # jackknife error (see above for exlpanation)
    imPolyErrJNm = np.sqrt(nBlocks) * np.std(imPolysNm)

    return imPoly, imPolyErrJNm


def plaquette(data,blockSize,idx):

    nMeas = data.shape[0]
    nObs  = data.shape[1] 
    nBlocks = int(np.floor(nMeas/blockSize))
    nMeasCut = nBlocks*blockSize

    
    # N-1 - see above for suffix meaning
    plaqsNm = jackknifeBlockingLocalArrayPower(data[:nMeasCut,:],\
            blockSize,1,idx.iPlaquette);


    plaq = np.mean(plaqsNm)


    # jackknife error (see above for exlpanation)
    plaqErrJNm = np.sqrt(nBlocks) * np.std(plaqsNm)

    return plaq, plaqErrJNm






def absImPolyLoopSusc(orignalData,blockSize,idx):
    data = np.abs(orignalData[:,[idx.iImPoly]])
    return suscJacknifeBlocking(data,blockSize,0)

def imPolyLoopBinder(orignalData,blockSize,idx): # no abs in binder!!
    data = orignalData[:,[idx.iImPoly]]
    return BinderJacknifeBlocking(data,blockSize,0)


def plaqSusc(orignalData,blockSize,idx):

    return suscJacknifeBlocking(orignalData,blockSize,idx.iPlaquette)


def suscWithWeights(data,weights=None): # data must be 1D, as weights
    
    mean = np.average(data,weights = weights )
    meanOfSquares = np.average(data**2,weights = weights )

    return meanOfSquares - mean **2 
 




gaugeObs = [rePolyLoop,absImPolyLoop, absImPolyLoopSusc,
                     imPolyLoopBinder, plaquette, plaqSusc] 
fermObs = [bareLightCond, renLightCond1, ChiralSuscLight, 
                 absImQuarkNumberLight, absImLightNDiscSusc]

# DeForcrand wants to see imLightN2
fermObs_TC = [bareLightCond, renLightCond1, ChiralSuscLight, #Thesis Correction
        absImQuarkNumberLight, absImLightNDiscSusc,imLightN2] 


