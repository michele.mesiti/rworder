#!/usr/bin/env python3

import numpy as np
import rw_input_file_parser as ifp
import bootstrapblocking.multi_histogram as mh
import osservabili_rw_mhrw as orw
from sys import argv,stdout,stderr,exit

from gauge_and_fermion_correction import *

# constants for energy calculation:
c0 = 5/3
c1 = -1/12


bootStrap = False
if bootStrap:
    reqArgs = 7
else:
    reqArgs = 6

if len(argv) != reqArgs:
    stderr.write("Usage : {} file.set beta_min beta_max [bootstrap_steps] chunk_id nchunks [look in logs]\n".format(argv[0]))
    exit(1)

stdout.write("Reading {}\n".format(argv[1]))
ifp = ifp.analisiSetting(argv[1])

betaMin = float(argv[2])-0.001
betaMax = float(argv[3])+0.001
if bootStrap:
    bootstrapSteps = int(argv[4])
    ichunk = int(argv[5])
    nchunks = int(argv[6])
else :
    ichunk = int(argv[4])
    nchunks = int(argv[5])








stdout.write("Beta interval {}-{}\n".format(argv[2], argv[3]))

betas = []
logZetasMHRWstarts = []
energyDatas  = []

allFermionDatas = []
blockSizes = []
V4s = []
modes = []
nCopiess = []

stdout.write("\n\nREADING DATA FILES\n\n")

for beta in sorted(ifp.betaInfo.keys()):
    if beta > betaMin and beta < betaMax:
        runGroupInfo = ifp.betaInfo[beta].info
        runRecords   = ifp.betaInfo[beta].runRecords
        blockSize = runGroupInfo.block

        gaugeDatas = []
        fermionicDatas = []
        for runInfo in runRecords:

            fermionicSaneFileName = runInfo.fermionicFileName + correctionSuffix
            stdout.write("Reading {}\n".format(fermionicSaneFileName))
            fermionicDataSane = np.loadtxt(fermionicSaneFileName)
            fermionicDatas.append(fermionicDataSane)
            
            gaugeSaneFileName = runInfo.gaugeFileName + correctionSuffix
            stdout.write("Reading {}\n".format(gaugeSaneFileName    ))
            gaugeDataSane = np.loadtxt(gaugeSaneFileName)
            gaugeDatas.append(gaugeDataSane)

            if not checkGaugeFermConsistency(gaugeDataSane,fermionicDataSane,runGroupInfo.nFermCopie,runGroupInfo.mode):
                gaugeDataSane, fermionicDataSane = alignGaugeAndFermionData(\
                        gaugeDataSane,fermionicDataSane,runGroupInfo.nFermCopie,\
                        runGroupInfo.mode)

            stderr.write("beta: {}, mode: {}, thermalization traj {}\n".format(beta, runGroupInfo.mode, runInfo.therm))
            if not checkGaugeFermConsistency(gaugeDataSane,fermionicDataSane,\
                    runGroupInfo.nFermCopie,runGroupInfo.mode):
                stderr.write("\n\nbeta: {} runId: {}, Gauge and Fermion file are not aligned!\n\n\n".format(beta,runInfo.runIndice))
                stdout.write("\n\nbeta: {} runId: {}, Gauge and Fermion file are not aligned!\n\n\n".format(beta,runInfo.runIndice))
            
        gaugeData = np.vstack(gaugeDatas)
        fermionicData = np.vstack(fermionicDatas)

        idx = orw.indexDictionary(runGroupInfo.mode)
        orw.verbose = False
        
        energyData = - runGroupInfo.volume * 6 * ( c0 * gaugeData[:,idx.iPlaquette] \
                + 2* c1 * gaugeData[:,idx.iRectangle]) 

        betas.append(beta)
        logZetasMHRWstarts.append(runGroupInfo.logZetaMHRWstart)
        energyDatas.append(energyData)
        allFermionDatas.append(fermionicData)
        blockSizes.append(blockSize)

        # these must be all equal
        V4s.append(runGroupInfo.volume)
        modes.append(runGroupInfo.mode)
        nCopiess.append(runGroupInfo.nFermCopie)


if len(betas) == 0:
    stderr.write("NO BETA SELECTED, CHECK RANGES!\n")
    exit(1)

betas = np.array(betas)

V4s =  set(V4s)
modes = set(modes)
nCopiess = set(nCopiess)

if len(V4s) != 1 or len(modes) != 1 or len(nCopiess) != 1:
    stderr.write("ERROR: NOT ALL RUNS HAVE THE SAME V4,mode and nCopies!")
    stderr.write("\nV4s (values found):")
    stderr.write(V4s)
    stderr.write("\nnCopiess (values found):")
    stderr.write(nCopies)
    stderr.write("\nmodes (values found):")
    stderr.write(modes)
    stderr.write("\nEXITING NOW\n")
    exit(1)

V4 =  V4s.pop()
mode = modes.pop()
nCopies = nCopiess.pop()

logZetasFileName = argv[1] + 'logZetas.dat'

stdout.write("Calculating logZetas...")
logZetas = mh.calcLogZetas(betas,energyDatas,logZetasMHRWstarts)

logZetasDict = dict()
for beta,logZeta in zip(betas,logZetas):
    logZetasDict[beta]=logZeta

stdout.write("Saving new setting file...\n")
if argv[1][-8:] == '.mod.set':
    newInputFileName = argv[1]
else:
    newInputFileName = argv[1].replace('.set','.mod.set')
ifp.writeInputFile(newInputFileName,logZetasDict)

stdout.write("\n\nNOW, REWEIGHTING OBSERVABLES...\n\n")

betaStep = 0.0005
tolerance = 1e-7

# FERMIONS

otherObsFunArgs = nCopies,V4,idx
funNames = [ obsFun.__name__ for obsFun in orw.fermObsMHRW ] 


print("n fermdatas: {}\n".format(len(allFermionDatas)))

bootStrap = False
if bootStrap:
    betaPoints, funMeans, funMeanErr =\
        mh.calcObsBootstrap(betas,energyDatas, logZetas,orw.fermObsMHRW,\
        otherObsFunArgs,allFermionDatas, blockSizes, betaMin, betaMax, betaStep,\
        bootstrapSteps,tolerance,nCopies)
else:
    pickleDump =\
        mh.calcObsJK(betas,energyDatas, logZetas,orw.fermObsMHRW,\
        otherObsFunArgs,allFermionDatas, blockSizes, betaMin, betaMax, betaStep,\
        tolerance,nCopies,ichunk,nchunks)


pickleDumpFileName =  argv[1] + 'chunk' + str(ichunk) + 'of' + str(nchunks) + '.PICKLE'
stdout.write("Writing file {}\n".format(pickleDumpFileName))
pickleDumpFile = open(pickleDumpFileName,'wb')
pickleDumpFile.write(pickleDump)
pickleDumpFile.close()
       










