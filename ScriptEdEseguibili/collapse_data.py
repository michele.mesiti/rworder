#!/usr/bin/env python3

import numpy as np
from scipy.optimize import leastsq
from sys import argv,stdout,stderr
from os import path
import math

stdout.write("COLLAPSING DATA SETS\n")
xMax = 3.3840

# first order exponents
nu1st = 1.0/3
gamma1st = 1
#coeffs_1st = (nu1st,gamma1st)

# ising3D exponents, https://arxiv.org/abs/cond-mat/0012164
nu2nd = 0.6301
gamma2nd = 1.2372
#coeffs_2nd = (nu2nd,gamma2nd)

# tricrit exponents, lavagna
nutric = 1.0/2.0
gammatric = 1
#coeffs_tric = (nutric,gammatric)

# Name of columns in files
iImPolSuscRW    = 5
iImPolSuscErrRW = 6

iImPolSusc    = 15
iImPolSuscErr = 16

iLightChSusc    = 5 
iLightChSuscErr = 6 

iLightChSuscRW    = 5 
iLightChSuscErrRW = 6 

def extractBetaMax(betaMaxFileName):
    ''' This function takes a file with the format like

# observable                     betamax              error
absImPolyLoopSuscWeightedNoJN   3.367988341968921   0.012850109909079957
plaqSuscSuscWeightedNoJN    3.368589378238352   0.013023280852226041
    
    and gets the betamax and error from the line containing
    'absImPolyLoopSuscWeightedNoJN'.'''
    stdout.write("Reading "+betaMaxFileName+'\n')
    table = np.genfromtxt(betaMaxFileName, dtype=None) 
    for line in table:
        if line[0].decode('utf-8') == 'absImPolyLoopSuscWeightedNoJN':
            return float(line[1]) , float(line[2])


# Setting file parsing 
settingFileName = argv[1]
stdout.write("Reading {}\n".format(settingFileName))
table = np.genfromtxt(settingFileName, dtype=None)

ls =              [ int(line[0]) for line in table ]
nts =             [ int(line[1]) for line in table ]
dataRWFileNamesGauge = [ line[2].decode('utf-8') for line in table ]
dataRWFileNamesFermion = [ line[3].decode('utf-8') for line in table ]
dataFileNames =   [ line[4].decode('utf-8') for line in table ]
dataBetaPeakFileNames =   [ line[5].decode('utf-8') for line in table ]

# end of setting file parsing

# reading maxima location from files
betaMaxAndErrs = [ 
        extractBetaMax(fileName) for fileName in dataBetaPeakFileNames ]


print(betaMaxAndErrs)

# fit the maxima and get an estimate for the critical beta at infinite volume
# for the three cases: first order, 2nd order ising and tricritical
betas_max_Vinf = [] # will contain 1st order,2nd order and tricritical est
tmp = np.array(betaMaxAndErrs)
betasMax, errs, ls = tmp[:,0], tmp[:,1], np.array(ls)


betaVinfFileName = 'betaVinfFitResults_'+argv[1]
stdout.write('Writing {}\n'.format(betaVinfFileName))
fBetaVinfFile = open(betaVinfFileName,'w')
fBetaVinfFile.write("#tag, nu,betaMaxVinf,coeff,chi2,ddof\n")


for nu,tag in [ (nu1st,'1st'), (nu2nd,'2nd'), (nutric,'tric') ]: 
    # fitting according to
    # beta_max(L) = beta_max_Vinf + coeff * L^(-1/\vu) 
    betaMaxVinf = 3.3850
    coeff = 1.0000
    p0 = (betaMaxVinf,coeff)

    #no worries! Gli scope in python sono diversi da come funzionano in C!
    # See 'LEGB' rules
    def residuals(pars,ls,betasmax,betasmax_err,nu ):
        #ls,betasmax,betasmax_err,nu = args
        betainf, coeff = pars
        betasmax_teo = betainf + coeff * ls**(-1/nu) # SCALING
        return (betasmax_teo - betasmax)/betasmax_err
        
    leastsq_results = leastsq(residuals,p0,(ls,betasMax,errs,nu),full_output=1)
    residuals_evaluated = leastsq_results[2]['fvec']
    ddof = residuals_evaluated.size - 2
    chi2 = np.sum(residuals_evaluated**2)
    betaMaxVinf,coeff = leastsq_results[0]
    
    stdout.write(("File {}, For nu = {}, betaMaxVinf={},"+
                                 " coeff={}, chi2/ndf={}/{}\n")
                    .format(argv[1],nu,betaMaxVinf,coeff,chi2,ddof))
    # 
    fBetaVinfFile.write(("{}\t"*6+"\n").
            format(tag,nu,betaMaxVinf,coeff,chi2,ddof))

    betas_max_Vinf.append(betaMaxVinf)

fBetaVinfFile.close()

betaMaxVinf1st,betaMaxVinf2nd,betaMaxVinfTric = betas_max_Vinf

def doAll(dataRWFileNames, dataFileNames, i, iErr, iRW, iErrRw, label,
        betaMaxVinf, volmult = True):

    for l,nt,dataRWFileName,dataFileName in zip(ls,nts,dataRWFileNames,dataFileNames):
        print(dataRWFileName)
        print(dataFileName)
 
        volfactor=1
        if volmult:
            volfactor = l**3
        print(l)
        print(volfactor)
            
        if dataRWFileName != 'MISSING':
            stdout.write("Reading {}\n".format(dataRWFileName))
            rwData = np.loadtxt(dataRWFileName)
            
            x = rwData[:,0]
            varSusc = rwData[:,iRW]
            varSuscErr = rwData[:,iErrRw]

            # MHRW rescaling
            #1st order
            x1st = (x-betaMaxVinf1st) * l **(1/nu1st) 
            varSusc1st = varSusc* l ** (-gamma1st/nu1st)      * volfactor * nt  
            varSuscErr1st = varSuscErr* l ** (-gamma1st/nu1st)* volfactor * nt     
        
            #2nd order
            x2nd = (x-betaMaxVinf2nd) * l **(1/nu2nd) 
            varSusc2nd = varSusc* l ** (-gamma2nd/nu2nd)      * volfactor * nt  
            varSuscErr2nd = varSuscErr* l ** (-gamma2nd/nu2nd)* volfactor * nt      

            #tric phase trans
            xtric = (x-betaMaxVinfTric) * l **(1/nutric) 
            varSusctric = varSusc* l ** (-gammatric/nutric)      * volfactor * nt  
            varSuscErrtric = varSuscErr* l ** (-gammatric/nutric)* volfactor * nt      

        
            outputFileHeader =  'x1st, varSusc1st, err, '
            outputFileHeader += 'x2nd, varSusc2nd, err, '
            outputFileHeader += 'xtric, varSusctric, err, '
            
            x1stOrd = np.argsort(x1st) 
            x2ndOrd = np.argsort(x2nd)
            xtricOrd = np.argsort(xtric)
            dataToWrite = np.vstack((x1st[x1stOrd],varSusc1st[x1stOrd],varSuscErr1st[x1stOrd],\
                    x2nd[x2ndOrd],varSusc2nd[x2ndOrd],varSuscErr2nd[x2ndOrd],\
                    xtric[xtricOrd],varSusctric[xtricOrd],varSuscErrtric[xtricOrd])).T
        
            dataToWriteFileName = path.basename(dataRWFileName) + label +"_RWCollapse.dat"
            stdout.write("Writing {}\n".format(dataToWriteFileName))
 
            np.savetxt(dataToWriteFileName,dataToWrite,header = outputFileHeader)




        # data points rescaling
        stdout.write("Reading {}\n".format(dataFileName))
        pointData = np.loadtxt(dataFileName)

        ##### THIS MUST BE CORRECTED OR MOVED #####
        #if dataRWFileName == 'MISSING' : # finding the maximum
        #    iMax = np.argmax(pointData[:,i])
        #    xMax = pointData[iMax,0]
        #    yMax = pointData[iMax,i]
        #    stdout.write("MISSING MHRW result file for {}, finding beta_max with fit\n".\
        #            format(dataFileName))
        #    stdout.write("       y in column {}, betalow {} betahigh {}\n".\
        #            format(i,pointData[iMax-2,0],pointData[iMax+3,0]))
        #    conc = -1
        #    p0 = (xMax,yMax,conc)
        #    dataToFit  = pointData[iMax-2:iMax+3,[0,i,iErr]]
        #    xMax,yMax,conc = leastsq(residuals,p0,dataToFit)[0]
        #    stdout.write("       betamax = {}\n".format(xMax))


        x = pointData[:,0]

        varSusc = pointData[:,i]
        varSuscErr = pointData[:,iErr]
     
        #1st order
        x1st = (x - betaMaxVinf1st) * l **(1/nu1st) 
        varSusc1st = varSusc* l ** (-gamma1st/nu1st)       * volfactor * nt  
        varSuscErr1st = varSuscErr* l ** (-gamma1st/nu1st) * volfactor * nt     
    
        #2nd order
        x2nd = (x - betaMaxVinf2nd) * l **(1/nu2nd) 
        varSusc2nd = varSusc* l ** (-gamma2nd/nu2nd)       * volfactor * nt  
        varSuscErr2nd = varSuscErr* l ** (-gamma2nd/nu2nd) * volfactor * nt     

        #tricritical
        xtric = (x - betaMaxVinfTric) * l **(1/nutric) 
        varSusctric = varSusc* l ** (-gammatric/nutric)       * volfactor * nt  
        varSuscErrtric = varSuscErr* l ** (-gammatric/nutric) * volfactor * nt     

        outputFileHeader =  'x1st, varSusc1st, err, '
        outputFileHeader += 'x2nd, varSusc2nd, err, '
        outputFileHeader += 'xtric, varSusctric, err,'
        dataToWrite = np.vstack((x1st,varSusc1st,varSuscErr1st,\
                x2nd,varSusc2nd,varSuscErr2nd,xtric,varSusctric,varSuscErrtric)).T
    
        dataToWriteFileName = path.basename(dataFileName) + label + "_Collapse.dat"
        stdout.write("Writing {}\n".format(dataToWriteFileName))
        np.savetxt(dataToWriteFileName,dataToWrite,header = outputFileHeader)
    


doAll(dataRWFileNamesGauge,dataFileNames,iImPolSusc,iImPolSuscErr,iImPolSuscRW,iImPolSuscErrRW,'_imPolsusc',betaMaxVinf,True)
doAll(dataRWFileNamesFermion,dataFileNames,iLightChSusc,iLightChSuscErr,iLightChSuscRW,iLightChSuscErrRW,'_LightChSusc',betaMaxVinf,False)
