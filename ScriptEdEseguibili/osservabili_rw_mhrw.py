#!/usr/bin/env python3
# This python library is provided to analyse the data produced by YALQCDSS.
# This is expected to work around commit 3c65d2ca3530693d941891807a7c1702e7c47185 
# of YALQCDSS.

import numpy as np
from bootstrapblocking.jackknife_blocking import * # deprecated, but who gives a fuck
from sys import stderr

from osservabili_rw import discPieceType1

from all_indices import *

# all observables functions for mhrw must have the form
#
# obsFun(allObss,weights,otherObsFunArgs)
#
# where 
# - allObs is a 2D array containing the data [nTotMeas][nObs]
# - weights is a 1D array [nTotMeas]
# - otherObsFunArgs is a list of all the other arguments needed my the function
#
# (SEE function calcObsBootstrap in multihistogram.py)


# Fermionic composed observables

verbose = False


def ChiralSuscLightWeightedNoJN(data,weights,args):   # data is [nMeas*nCopies,nObs]
    
    nCopies, V4,idx = args

    nObs  = data.shape[1] 

    if verbose:
        stderr.write("nObs {}\n".format(nObs))

    argsForDdisc1 = (nCopies,(idx.iUpReChCond,idx.iDownReChCond))

    dataReshaped = np.reshape(data,(-1,nCopies*nObs))


    # N blocks - NO JACKKNIFE
    # using data[:nMeasCut*nCopies,:], the leftovers cannot be used.
    # 1
    lightDisc1N = V4 * np.average(discPieceType1(dataReshaped,argsForDdisc1),\
            weights = weights)

    # 2
    upDisc2Ns   = np.mean(data[:,idx.iUpReChCond]  .reshape((-1,nCopies)),axis = 1)
    downDisc2Ns = np.mean(data[:,idx.iDownReChCond].reshape((-1,nCopies)),axis = 1)

    upDisc2N   = np.average(upDisc2Ns   ,weights = weights)
    downDisc2N = np.average(downDisc2Ns ,weights = weights)

    # we want to take the square, but we need to get rid of contact terms
    # see consideration above on the negligibility of contact terms in this case

    # averaging over light quarks
    lightDisc2N = ((upDisc2N + downDisc2N)/2)**2


    # notice : a V4 factor is missing, so 
    lightDisc2N *= -V4



    # 3 

    upConnNs   = np.mean(data[:,idx.iUpReChSuscConn]  .reshape((-1,nCopies)),axis = 1)
    downConnNs = np.mean(data[:,idx.iDownReChSuscConn].reshape((-1,nCopies)),axis = 1)

    upConnN   = np.average(upConnNs   ,weights = weights)
    downConnN = np.average(downConnNs ,weights = weights)
    lightConnN = (upConnN + downConnN)/2

    chSuscLightN = lightDisc1N + lightDisc2N + lightConnN 

    if verbose :
    
        stderr.write( "lightDisc1N {},  lightDisc2N {}, lightConnN {}\n".format(lightDisc1N, lightDisc2N , lightConnN)) 
        stderr.write("chSuscLightN {}\n".format(chSuscLightN))
    
    return chSuscLightN


def absImLightNDiscSuscWeightedNoJN(data,weights,args):   # data is [nMeas*nCopies,nObs]
    
    nCopies, V4,idx = args

    nObs  = data.shape[1] 

    if verbose:
        stderr.write("nObs {}\n".format(nObs))

    argsForDdisc1 = (nCopies,(idx.iUpReChCond,idx.iDownReChCond))

    dataReshaped = np.reshape(data,(-1,nCopies*nObs))
    dataImLightNOnly = data[:,[idx.iUpImN,idx.iDownImN]]
    dataReshapedImLightNOnly = np.reshape(dataImLightNOnly, (-1,nCopies*2))
                                                 # 2  = no of indices chosen
                                                 # UP-DOWN total degeneracy IS ASSUMED!
    imLightNOnlyNoiseMeans = np.mean(dataReshapedImLightNOnly, axis = 1 )
    
    absImLightNOnlyNoiseMeans = np.abs(imLightNOnlyNoiseMeans) # 2 because of n_up+n_down


    # 1
    lightDisc1N = V4 * np.average(discPieceType1(dataReshaped,argsForDdisc1),\
            weights = weights)


    # 2
    # we have to take the noise means and then take the absolute value
    # averaging over light quarks
    lightDisc2N = np.average(absImLightNOnlyNoiseMeans, weights = weights) **2


    # notice : a V4 factor is missing, so 
    lightDisc2N *= - V4
 
    QNDiscSuscLightN = lightDisc1N + lightDisc2N
    
    return QNDiscSuscLightN



   


def bareLightCondWeightedNoJN(data,weights,args):   # data is [nMeas*nCopies,nObs]
                                       #V4 is useless, but keep for uniformity
                                       # with the other fermionic variables
                                       # <uu> + <dd>
    nCopies, V4,idx = args

    nObs  = data.shape[1] 

    if verbose:
        stderr.write("nObs {}\n".format(nObs))
        stderr.write("idx.iUpReChCond: {} , idx.iDownReChCond {}\n".format(idx.iUpReChCond, idx.iDownReChCond))


    dataChCondLightOnly = data[:,[idx.iUpReChCond,idx.iDownReChCond]]

    dataReshapedChCondLightOnly = np.reshape(dataChCondLightOnly, (-1,nCopies*2))
                                                 # 2  = no of indices chosen
                                                 # UP-DOWN total degeneracy IS ASSUMED!

    lightConds = 2*np.mean(dataReshapedChCondLightOnly, axis = 1 ) # 2 because it's uu + dd



    # observable is very simple and unbiased.
    lightCond = np.average(lightConds, weights = weights)
    

    return lightCond


def absImQuarkNumberLightWeightedNoJN(data,weights,args): # data is [nMeas*nCopies,nObs]
                                       #V4 is useless, but keep for uniformity
                                       # with the other fermionic variables
                                       # <uu> + <dd>
    nCopies, V4,idx = args

    nObs  = data.shape[1] 

    if verbose:
        stderr.write("nObs {}\n".format(nObs))
        stderr.write("idx.iUpImN: {} , idx.iDownImN {}\n".format(idx.iUpImN, idx.iDownImN))

    dataImLightNOnly = data[:,[idx.iUpImN,idx.iDownImN]]

    dataReshapedImLightNOnly = np.reshape(dataImLightNOnly, (-1,nCopies*2))
                                                 # 2  = no of indices chosen
                                                 # UP-DOWN total degeneracy IS ASSUMED!

    imLightNOnlyNoiseMeans = np.mean(dataReshapedImLightNOnly, axis = 1 )
    
    absImLightNOnlyNoiseMeans = np.abs(imLightNOnlyNoiseMeans) # 2 because of n_up+n_down

    # observable is very simple and unbiased.

    absImLightN = np.average(absImLightNOnlyNoiseMeans, weights = weights)


    return absImLightN





def renLightCond1WeightedNoJN(data,weights,args):   # data is [nMeas*nCopies,nObs]
                                       #V4 is useless, but keep for uniformity
                                       # with the other fermionic variables

    # <uu> + <dd> - 2m_l/m_s <ss>

    mass_ratio = 1/28.15

    nCopies, V4,idx = args

    nObs  = data.shape[1] 

    if verbose:
        stderr.write("nObs {}\n".format(nObs))
        stderr.write("idx.iUpReChCond: {} , idx.iDownReChCond {} idx.iStrangeReChCond {}\n".\
format(idx.iUpReChCond, idx.iDownReChCond, idx.iStrangeReChCond))

    dataChCondLightOnly = data[:,[idx.iUpReChCond,idx.iDownReChCond]]

    dataReshapedChCondLightOnly = np.reshape(dataChCondLightOnly, (-1,nCopies*2))
                                                 # 2  = no of indices chosen
                                                 # UP-DOWN total degeneracy IS ASSUMED!

    lightConds = np.mean(dataReshapedChCondLightOnly, axis = 1 )
       
    dataReshapedChCondStrangeOnly = np.reshape(data[:,idx.iStrangeReChCond],(-1,nCopies))
    
    strangeConds = np.mean(dataReshapedChCondStrangeOnly, axis = 1 )
    
    lightRenChConds = 2*lightConds  - 2* mass_ratio * strangeConds

    lightRenChCond = np.average(lightRenChConds, weights = weights)

    return lightRenChCond



def rePolyLoopWeightedNoJN(data,weights,args):

    idx = args

    rePoly = np.average(data[:,idx.iRePoly],weights = weights)

    return rePoly


def absImPolyLoopWeightedNoJN(data,weights,args):

    idx = args
    
    imPoly = np.average(np.abs(data[:,idx.iImPoly]),weights = weights)

    return imPoly


def plaquetteWeightedNoJN(data,weights,args):

    idx = args
    
    plaq = np.average(data[:,idx.iPlaquette],weights = weights)

    return plaq



def suscWithWeights(data,weights=None): # data must be 1D, as weights
    
    mean = np.average(data,weights = weights )
    meanOfSquares = np.average(data**2,weights = weights )

    return meanOfSquares - mean **2 

def binderWithWeights(data,weights=None): # data must be 1D, as weights
    
    meanOfQuartics = np.average(data**4,weights = weights )
    meanOfSquares = np.average(data**2,weights = weights )

    return meanOfQuartics/meanOfSquares**2 


def absImPolyLoopSuscWeightedNoJN(data,weights,args):

    idx = args
    return suscWithWeights(np.abs(data[:,idx.iImPoly]),weights)


def imPolyLoopBinderWeightedNoJN(data,weights,args):

    idx = args
    return binderWithWeights(data[:,idx.iImPoly],weights)




def plaqSuscSuscWeightedNoJN(data,weights,args):

    idx = args
    return suscWithWeights(np.abs(data[:,idx.iPlaquette]),weights)


gaugeObsMHRW = [rePolyLoopWeightedNoJN,absImPolyLoopWeightedNoJN,\
        absImPolyLoopSuscWeightedNoJN, imPolyLoopBinderWeightedNoJN,\
        plaquetteWeightedNoJN,plaqSuscSuscWeightedNoJN] 
fermObsMHRW  = [bareLightCondWeightedNoJN, renLightCond1WeightedNoJN,\
        ChiralSuscLightWeightedNoJN, absImQuarkNumberLightWeightedNoJN,\
        absImLightNDiscSuscWeightedNoJN ]


