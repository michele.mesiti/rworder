#!/usr/bin/env python3

import numpy as np
import rw_input_file_parser as parser
from sys import argv,exit,stdout, stderr
#from matplotlib import pyplot as plt
from os import mkdir


import osservabili_rw as orw
from gauge_and_fermion_correction import *

stdout.write("Reading {}\n".format(argv[1]))
ifp = parser.analisiSetting(argv[1])

dirout =  argv[1] + '_Histograms'
stdout.write("Creating dir {}\n".format(dirout))

try:
    mkdir(dirout)
except OSError:
    stdout.write("Dir {} already exists!\n".format(dirout))


beta = float(argv[2])

runGroupInfo = ifp.betaInfo[beta].info
runRecords   = ifp.betaInfo[beta].runRecords

gaugeDatas = []
fermionicDatas = []
for runInfo in runRecords:

    fermionicSaneFileName = runInfo.fermionicFileName + correctionSuffix
    stdout.write("Reading {}\n".format(fermionicSaneFileName))
    fermionicDataSane = np.loadtxt(fermionicSaneFileName)
    fermionicDatas.append(fermionicDataSane)
    
    gaugeSaneFileName = runInfo.gaugeFileName + correctionSuffix
    stdout.write("Reading {}\n".format(gaugeSaneFileName    ))
    gaugeDataSane = np.loadtxt(gaugeSaneFileName)
    gaugeDatas.append(gaugeDataSane)
    
    stderr.write("beta: {}, mode: {}, thermalization traj {}\n".format(beta, runGroupInfo.mode, runInfo.therm))




gaugeData = np.vstack(gaugeDatas)
fermionicData = np.vstack(fermionicDatas)

# SELECTING  FERMIONIC FILE MODE
idx = orw.indexDictionary(runGroupInfo.mode)
orw.verbose = False

# creating histograms of Im poly, plaquette, light chiral condensate

plqData = gaugeData[:,idx.iPlaquette]
imPolyData = gaugeData[:,idx.iImPoly]
if idx.iUpReChCond != idx.iDownReChCond:
    lightChCondData = fermionicData[:, [idx.iUpReChCond,idx.iDownReChCond ]]
    lightChCondData = np.reshape(lightChCondData,(-1, runGroupInfo.nFermCopie*2))
    # 2 because of the quark number
    lightChCondData = 2*np.mean(lightChCondData, axis = 1)

else: 
    lightChCondData = fermionicData[:, idx.iUpReChCond ]
    lightChCondData = np.reshape(lightChCondData,(-1, runGroupInfo.nFermCopie))
    # 2 because of the quark number
    lightChCondData = np.mean(lightChCondData, axis = 1)


for obsName,data in [('plaquette',plqData),\
        ('imPoly',imPolyData),\
        ('lCond',lightChCondData)]:
   

    nbins = int(np.sqrt(data.size))
    y,x = np.histogram(data, bins = nbins,normed=True)
    # unfortunately, x is the list of bin edges
    y = np.repeat(y, 2)
    xm = (x[:-1] + x[1:])/2
    x = np.hstack((x[0], np.repeat(x[1:-1],2), x[-1]))
    xm = np.repeat(xm, 2)

    arrToSave = np.vstack((x,y,xm)).T

    outfileName = dirout + '/' + 'H' +obsName+ '.'+ str(beta)+ '.dat'
    stdout.write("Writing file {}\n".format(outfileName))
    np.savetxt(outfileName, arrToSave)









   




