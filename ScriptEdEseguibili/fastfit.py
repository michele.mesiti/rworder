#!/usr/bin/env python3

import numpy as np
from sys import exit,argv,stdout,stdin,stderr
import select
import numpy as np
import argparse as ap

import matplotlib
#matplotlib.rcParams['backend'] = "Qt4Agg"

import matplotlib.pyplot as plt

bres = 5000 # no. of bootstrap resaplings
e = 1       # parameter to manually reduce errors, change for debug/testing
            # purposes only

immumode = False


def quadfit(x,y,dy):
    B2 = np.sum(y*x**2/dy**2)
    B1 = np.sum(y*x/dy**2)
    B0 = np.sum(y/dy**2)

    B = np.array([B2,B1,B0]).reshape(3,1)
    
    A0 = np.sum(1/dy**2)
    A1 = np.sum(x/dy**2)
    A2 = np.sum(x**2/dy**2)
    A3 = np.sum(x**3/dy**2)
    A4 = np.sum(x**4/dy**2)

    M = np.matrix( [
      [ A4 , A3 ,A2 ],
      [ A3 , A2 ,A1 ],
      [ A2 , A1 ,A0 ]
      ])

    a,b,c = np.array(M.I * B).flatten() #parameters

    residuals = (y - a*x**2 -b*x -c )/dy
    chi = np.sum(residuals**2)

    ndof = len(x) - 3
    if immumode:
        return (a/c/(81*3.141592**4),b/c/(9*3.141592**2),c) , chi , ndof
    else:
        return (a,b,c) , chi , ndof


"""
Minimizing the quantity
\begin{equation}
\chi^2 = \sum_i \left[\frac{y_i - (a x_i + b)}{dy^2} \right]^2
\end{equation}
with respect to $a$ and $b$ yields the following equations:
\begin{eqnarray}
a \sum_i \frac{x_i^2}{dy_i^2} + b \sum_i \frac{y_i x_i}{dy_i^2} & = & \sum_i \frac{y_i x_i }{dy_i^2} \\
a \sum_i \frac{x_i}{dy_i^2} + b \sum_i \frac{1}{dy_i^2} & = & \sum_i \frac{y_i}{dy_i^2} \\
\end{eqnarray}
Then, redefining the system equation in the form
\begin{eqnarray}
A11 a + A12 b & = & B1 \\
A21 a + A22 b & = & B2
\end{eqnarray}
Note that $A21 = A12$.
"""

def linfit(x,y,dy):

    A11 = np.sum( (x/dy)**2)
    A12 = np.sum( x / (dy**2))
    B1  = np.sum( (x * y) / (dy**2))
    A21 = A12
    A22 = np.sum( 1.0 / (dy**2))
    B2  = np.sum( y / (dy**2))

    Determinant = A11*A22 - A12*A21

    if Determinant == 0 :
        print("Determinant is zero! Exiting now.")
        exit(1)
    
    a  =(B1*A22 - B2*A12) / Determinant 
    b  =-(B1*A12 - B2*A11) / Determinant 
    chisquare = np.sum(((y - a*x - b)/dy)**2)
    ndof = len(x)-2

    if immumode:
        return (b, a/b/(9*3.141592**2)), chisquare , ndof
    else:
        return (a,b),chisquare, ndof

def quadfit_symm(x,y,dy):
    return linfit(x**2,y,dy)

ppinfo = [   # parameter parsing information
# name/flag, type, default value, help
['-filename',str,None,'Input file name (not necessary if in pipe mode)'],
['-e',int,1,'Deviation control parameter, default 1'],
['-bres', int, 5000, 'Number of bootstrap resamples, default 5000'],
['-xcol',int,0,'X column in input file, default 0'],
['-ycol',int,1,'Y column in input file, default 1'],
['-dycol',int,2,'dY column in input file, default 2'],
]

pars = ap.ArgumentParser("Fast fit with bootstrap.")
for i in range(len(ppinfo)):
    pars.add_argument(ppinfo[i][0], type=ppinfo[i][1], default=ppinfo[i][2],
                  help=ppinfo[i][3])

pars.add_argument('-v',action = 'count', default = 0, help = 'Verbose mode' )#verbosity
pars.add_argument('-immu',action = 'count', default = 0, help = 'Fit in IMMU mode' )#form of the fits
pars.add_argument('-p',action = 'count', default = 0, help = 'Plot mode' )#plot or not?
pars.add_argument('-q',action = 'count', default = 0, help = 'quadratic fit mode' )#linear (default) or quadratic?
pars.add_argument('-qs',action = 'count', default = 0, help = 'quadratic fit mode, symmetric (ax**2+b)' )#linear (default) or quadratic?
pars.add_argument('-ao',action = 'count', default = 0, help = 'writes all output' )#writes all files in output
pars.add_argument('-tozero',action = 'count', default = 0, help = 'plots/calculates data up to zero (useful for continuum limits)' )#writes all files in output


if __name__ == '__main__':

    args = pars.parse_args()
    filename = args.filename
    e        = args.e
    bres     = args.bres
    xcol     = args.xcol
    ycol     = args.ycol
    dycol    = args.dycol

    doplot = bool(args.p)
    verbose = bool(args.v)
    doao = bool(args.ao)# writes output files
    immumode = bool(args.immu)    
    tozero = bool(args.tozero)    


    if args.q == 0:
        if args.qs ==0 :
            mode = 'lin'
        elif args.qs == 1:
            mode = 'quadsym'
        else:
            stderr.write('Invalid fit mode!\n')
            exit(1)
    elif args.q == 1:
        if args.qs ==0 :
            mode = 'quad'
        else:
            stderr.write('Invalid fit mode!\n')
            exit(1)
    else:
        stderr.write('Invalid fit mode!\n')
        exit(1)
    if verbose:
        stderr.write('Mode %s\n' % mode)


    pipe_mode = select.select([stdin,],[],[],0.0)[0] #
    if pipe_mode:
        t = np.loadtxt(stdin,dtype = np.float)[:,[xcol,ycol,dycol]]
    else:
        if filename is not None:
            t = np.loadtxt(filename)[:,[xcol,ycol,dycol]]
        else :
            stderr.write("No file or data given!\n")
            exit(1)
 
    p_s = []
    chi_s = []
    chi = 0
    for i in range(bres):
        stderr.write("Bootstrapping, step %d ...\r" % i)
        stderr.flush()
        temp_y = np.random.normal(t[:,1],e*t[:,2])
        if mode == 'lin':
            p,chi,ndof = linfit(t[:,0],temp_y,t[:,2])
        if mode == 'quad':
            p,chi,ndof = quadfit(t[:,0],temp_y,t[:,2])
        if mode == 'quadsym':
            p,chi,ndof = quadfit_symm(t[:,0],temp_y,t[:,2])

        p_s.append(p)
        chi_s.append(chi/ndof)
    stderr.write('\n')

    p_err = np.std(p_s,0)
    
    if mode == 'lin':
        p,chi,ndof = linfit(t[:,0],t[:,1],t[:,2])
        a,b = p
        a_err , b_err = p_err
        if immumode:
            str_fitform = 'a*(1+9*pi**2*b*x)'
        else:
            str_fitform = 'ax+b'
    if mode == 'quad':
        p,chi,ndof = quadfit(t[:,0],t[:,1],t[:,2])
        a,b,c = p
        a_err , b_err, c_err = p_err
        if immumode:
            str_fitform = 'c*(81*pi**4*a*x**2+9*pi**2*b*x+1)'
        else:
            str_fitform = 'ax**2+bx+c'
    if mode == 'quadsym':
        p,chi,ndof = quadfit_symm(t[:,0],t[:,1],t[:,2])
        a,b = p
        a_err , b_err = p_err
        if immumode:
            str_fitform = 'a*(1+9*pi**2*b*x**2)'
        else:
            str_fitform = 'ax**2+b'


    if verbose:
        print('Fit form: %s' % str_fitform )
        if mode == 'lin' or mode == 'quadsym':
            print('a\ta_err\tb\tb_err\tchi_red')
        if mode == 'quad':
            print('a\ta_err\tb\tb_err\tc\tc_err\tchi_red')
    if mode == 'lin' or mode == 'quadsym':
        print('%f\t%f\t%f\t%f\t%f'% (a,a_err,b,b_err,chi/ndof))
    if mode == 'quad':
        print('%f\t%f\t%f\t%f\t%f\t%f\t%f'% (a,a_err,b,b_err,c,c_err,chi/ndof))
    
    xmax = np.max(t[:,0])
    xmin = np.min(t[:,0])
    m = 0.15 *(xmax - xmin )
    step = 0.05 * (xmax - xmin)
    xmax += m
    xmin -= m
    if tozero:
        if xmin > 0:
            xmin = 0
        if xmax < 0:
            xmax = 0
    x = np.arange(xmin,xmax,step)
    if mode == 'lin':
        if immumode:
            yf = a*(1+b*9*3.141592**2*x)
        else:
            yf = a*x + b
    if mode == 'quad':
        if immumode:
            yf = c * ( a*81*3.141592**4*x**2 + b*9*3.141592**2*x +1 )
        else:
            yf = a*x**2 + b*x + c
    if mode == 'quadsym':
        if immumode:
            yf = a*(1+b*9*3.141592**2*x**2)
        else:
            yf = a*x**2 + b

    functable = np.vstack((x,yf)).T
    if doao and filename is None:
        stderr.write("No filename for output given, no output written\n")
    if doao and filename is not None:
        func_fileout_name = mode + 'fit_' + filename
        np.savetxt(func_fileout_name, functable)
    if doplot:
        plt.errorbar(t[:,0],t[:,1],yerr=t[:,2], linestyle = 'none')
        plt.plot(x,yf,label = mode)
        y_max = np.max(t[:,1]+t[:,2])
        y_min = np.min(t[:,1]-t[:,2])
        yf_max = np.max(yf)
        yf_min = np.min(yf)
        if y_max < yf_max:
            y_max = yf_max
        if y_min > yf_min:
            y_min = yf_min
        
        c = 0.15 *(y_max - y_min )
        x_max = np.max(t[:,0])
        x_min = np.min(t[:,0])
        if tozero:
            if x_min > 0:
                x_min = 0
            if x_max < 0:
                x_max = 0
        d = 0.15 *(x_max - x_min )
        d = 0.15 *(x_max - x_min )
        plt.ylim([y_min-c,y_max+c])
        plt.xlim([x_min-d,x_max+d])
        plt.legend()
        plt.show()








    

