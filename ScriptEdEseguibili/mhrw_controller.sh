#!/bin/bash
QUEUE=$3
FILE=$1
WORKDIR=$2

if  !  [ $FILE  ]
then
  echo Filename not given
  exit
fi
if ! [ -f $FILE ] 
then
   echo invalid file.
   exit
fi 
if  !  [ $WORKDIR  ]
then
  echo 'Working directory not given (maybe $PWD?)'
  exit
fi
if  !  [ -d $WORKDIR  ]
then
  echo 'Working directory ' $WORKDIR  ' does not exist'
  exit
fi

echo Reading $FILE

while read setfile betamin betamax fchunks gchunks
do

    for ichunk in `seq 0 $((fchunks-1))`
    do
        SCRIPTNAME=fermMHRW.$setfile.$ichunk.sh
        echo Writing $SCRIPTNAME ...
        echo cd $WORKDIR > $SCRIPTNAME
        echo log ../ScriptEdEseguibili/analisi_mhrw_fermions_pickle.py $setfile $betamin $betamax $ichunk $fchunks >> $SCRIPTNAME
        chmod +x $SCRIPTNAME
        echo Submitting script $SCRIPTNAME to queue $QUEUE
        echo bsub -q $QUEUE -eo out${SCRIPTNAME%.sh}.txt    $PWD/$SCRIPTNAME
        bsub -q $QUEUE -eo out${SCRIPTNAME%.sh}.txt    $PWD/$SCRIPTNAME
    done

    for ichunk in `seq 0 $((gchunks-1))`
    do
        SCRIPTNAME=gaugeMHRW.$setfile.$ichunk.sh
        echo Writing $SCRIPTNAME ...
        echo cd $WORKDIR > $SCRIPTNAME
        echo log ../ScriptEdEseguibili/analisi_mhrw_gauge_pickle.py $setfile $betamin $betamax $ichunk $gchunks >> $SCRIPTNAME
        chmod +x $SCRIPTNAME
        echo Submitting script $SCRIPTNAME to queue $QUEUE
        echo bsub -q $QUEUE -eo out${SCRIPTNAME%.sh}.txt    $PWD/$SCRIPTNAME
        bsub -q $QUEUE -eo out${SCRIPTNAME%.sh}.txt    $PWD/$SCRIPTNAME
    done
done < <(grep -v '^#' $FILE )






