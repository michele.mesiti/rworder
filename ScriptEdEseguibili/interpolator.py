#!/usr/bin/python3

import numpy as np
from scipy import interpolate
from sys import argv, exit
import fastfit

mode = ""
fastfit.immumode = False

def spl_boot(x,y,dy,x_spacing):

    step = x_spacing
    boot_max = 5000
    
    xnew = np.arange(x[0], x[-1], step)
    
    yboot = [] 
    
    for i in range(boot_max):
        ytemp = np.random.normal(y,dy)
        if mode == "spline":
            tck = interpolate.splrep(x,ytemp) 
            ynew = interpolate.splev(xnew,tck)
        if mode == "linfit":
            (a,b), chisquare , ndof = fastfit.linfit(x,ytemp,dy)
            ynew = a * xnew + b

        yboot.append(ynew)
        
    yboot_arr = np.array(yboot)
    ynew = np.mean(yboot_arr , 0 )
    yerr = np.std(yboot_arr , 0 )
    return xnew, ynew, yerr

if __name__ == '__main__':

    print("-Splinatore-")
    print("Guardate i parametri nello script prima di eseguire!")
    
    try:
        argv[2]
    except IndexError:
        print("Usage: %s %s %s " % (argv[0], "filename", "mode"))
        print("where mode is either \"spline\" or \"linfit\"")
        exit(1)

    mode = argv[2]

    table = np.loadtxt(argv[1])
    
    x = table[:,0]
    y = table[:,1]
    dy = table[:,2]
    
    xnew, ynew, yerr = spl_boot(x,y,dy,0.01)
    
    result = np.vstack((xnew,ynew,yerr)).T
    
    np.savetxt(mode + "_" + argv[1], result)
    
    
