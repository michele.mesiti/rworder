#!/usr/bin/python
#calculates the autocorrelation functions of a list of columns

import numpy as np
from scipy.optimize import curve_fit

# autocorr function
def autoCcorr(array, dmax):
    values = []
    means = []
    errors = []
    arraymean = np.mean(array)
    invarraysigma2 = np.std(array)**(-2)
    for d in range(dmax):
        values.append([])
        for index in range(len(array)-d):
            values[d].append(invarraysigma2*(array[index]-arraymean)*(array[index+d]-arraymean))
        means.append(np.mean(values[d]))
        errors.append(np.std(values[d])/(np.sqrt(len(array)-d)))
    return means, errors
    
#wiener process, for test
def generateRandomSeq(mean,delta,r,size):
    values = []
    values.append(np.random.normal(mean,delta))
    print("Expected variance:" , 0.5*delta**2/r)
    print("Expected autocorrelation time:", 1/r)
    for i in range(size-1):
        values.append(values[i]+ np.random.normal(-r*(values[i]-mean),delta))
    return values
   
def expf(x,a):
    return np.exp(-x*a)


# stupid blocking, for mean and error
def blockingMeanErr(data0,blockSize):

    '''
    Takes 1D array and block size and outputs mean and error
    '''
  
    sampleSize = data0.shape[0]
    
    mean = np.mean(data0)

    xblocks = sampleSize/blockSize 
    leftovers = sampleSize%blockSize 
    nblocks = int(np.ceil(xblocks))

#    print("block number: ",nblocks," lefovers:", leftovers, " xblocks:", xblocks)

    data = np.resize(data0, (nblocks,blockSize))

    blockMeans = np.mean(data,axis=1)
    
    if (leftovers) != 0:
        blockMeans[nblocks-1] = np.mean(data[nblocks-1,0:leftovers])
    

    blockedDataWeigths = np.zeros(nblocks) + blockSize
    if (leftovers) != 0:
        blockedDataWeigths[nblocks-1] = leftovers

#    print("Sigma of data: ",np.std(data))
#    print("Mean of data: ",mean)

    standardError = np.sqrt(np.average((blockMeans - mean )**2, weights = blockedDataWeigths)\
            / xblocks)
    
    return mean, standardError



# this function must be used when your observable "obsfun" has a strange,
# complex dependence on the whole sample. This is NOT likely to be the case.
# If your observable can be made up of "local" pieces, the next function is more appealing
# to you.
def jackknifeBlockingNonLocal(data,blockSize,NonLocalObsFun): 
    # data is bi-dimensional!! [nmeasures,nobs]
    # the montecarlo step varies along dimension 0 (rows) [nmeasures]
    # observable (or the copy) varies along dimension 1 (column) [nobs]
    # NonLocalObsFun() selects the right columns

    meanN = NonLocalObsFun(data) # we assume this estimate has a bias AlphaBias/N, that is
                         # RealValue = meanN + AlphaBias/N 

    sampleSize = data.shape[0]
    
    N =int(sampleSize/blockSize)

    vdata = data.view() 
    vdata = vdata[0:N*blockSize,:] # cutting off leftovers

    meansNm = []
    for i in range(0,N):
        tvdata = np.vstack((vdata[:i*blockSize,:],vdata[(i+1)*blockSize:,:]))
        meansNm.append(NonLocalObsFun(tvdata))
        #end for on blocks

    meanNm = np.mean(meansNm) # this is our estimate, which we assume has a bias
                              # AlphaBias / (N-1), so
                              # RealValue = meanNm + AlphaBias/(N-1)
    '''
    Since 

    RealValue = meanN  + AlphaBias /  N                                 [1.a] 
    RealValue = meanNm + AlphaBias /(N-1)                               [1.b]

    Then

    N     * RealValue = N     * meanN  + AlphaBias                      [2.a] 
    (N-1) * RealValue = (N-1) * meanNm + AlphaBias                      [2.b]         

    Subtracting 2.a-2.b

    RealValue = N * meanN - (N-1) * meanNm

    '''

    RealValue = N * meanN - (N-1) * meanNm

    return RealValue


# in general, for an observable that does not mix measures, you can use this,
# which produces and array nBlocks-long 
def jackknifeBlockingLocalArray(data,blockSize,localObsFun,localObsFunArgs): 
    # data is bi-dimensional!! [nMeas,(ncopies)*nobs]
    # the montecarlo step varies along dimension 0 (rows) [nMeas]
    # observable (or the copy) varies along dimension 1 (column) [(ncopies)*nObs]
    # LocalObsFun() selects the right columns and returns an array long [mMeas]
    # This function returns an [nblocks] long array

    obsResults = localObsFun(data,localObsFunArgs) # [nMeas]
    sampleSize = obsResults.shape[0]

    nBlocks = int(sampleSize/blockSize)
    sizeCut = nBlocks * blockSize


    dataReshaped = np.resize(obsResults[:sizeCut], (nBlocks,blockSize))

    mean = np.mean(dataReshaped)
    blockMeans = np.mean(dataReshaped,axis=1)  # mean on [block,:]
    return (mean*nBlocks - blockMeans)/(nBlocks-1) # [nBlocks]



#for x**exponent functions, which are "local" observables.
def jackknifeBlockingLocalArrayPower(data,blockSize,exponent,column): 
    # data is bi-dimensional!! [nmeasures,nobs]
    # the montecarlo step varies along dimension 0 (rows) [nmeasures]
    # observable (or the copy) varies along dimension 1 (column) [nobs]
    
    obsResults = data[:,column]**exponent
    sampleSize = obsResults.shape[0]

    xblocks = sampleSize/blockSize 
    nblocks = int(np.floor(xblocks)) #notice we can't use a non complete block
    sizeCut = nblocks * blockSize

    dataReshaped = np.resize(obsResults[:sizeCut], (nblocks,blockSize))

    mean = np.mean(dataReshaped)
    blockMeans = np.mean(dataReshaped,axis=1)  # mean on [block,:,obs]
    return (mean*nblocks - blockMeans)/(nblocks-1)


# bootstrap
def reshuffle(arr,indexes=None):
    # arr MUST be bi-dimensional [nmeasures,nobs]
    # for bootstrap resampling.
    # arr is a [nblocks][nwhatever] array
    nlines = arr.shape[0]
    
    if indexes is None:
        indexes = np.random.randint(0,nlines,nlines)

    return arr[indexes,:],indexes
    
#bootstrap, with blocks
def reshuffleBlock2D(arr,blockSize,indexes=None,nCopies=1):
    # arr MUST be bi-dimensional [nmeasures*nCopies,nobs]
    # for bootstrap resampling.
    # arr is a [nblocks][nwhatever] array
    nBlocks = int(np.floor(arr.shape[0]/(nCopies*blockSize)))
    cutArr = arr[:nBlocks*blockSize*nCopies,:] #notice we can't use a non complete block
    cutArr = np.reshape(cutArr,(nBlocks,blockSize*nCopies,arr.shape[1]))

    if indexes is None:
        indexes = np.random.randint(0,nBlocks,nBlocks)

    cutArr = cutArr[indexes,:,:]

    cutArr = np.reshape(cutArr,(nBlocks*blockSize*nCopies,arr.shape[1]))

    return cutArr,indexes

def reshuffleBlock1D(arr,blockSize,indexes=None):
    # arr MUST be 1-dimensional [nmeasures]
    # for bootstrap resampling.
    # arr is a [nblocks][nwhatever] array
    nBlocks = int(np.floor(arr.shape[0]/blockSize))
    cutArr = arr[:nBlocks*blockSize]  #notice we can't use a non complete block
    cutArr = np.reshape(cutArr,(nBlocks,blockSize))

    if indexes is None:
        indexes = np.random.randint(0,nBlocks,nBlocks)

    cutArr = cutArr[indexes,:]
    cutArr = np.reshape(cutArr,(nBlocks*blockSize))

    return cutArr,indexes


#jackknife, with blocks
def deleteBlock2D(arr,blockSize,blockIndex,nCopies=1):
    # arr MUST be bi-dimensional [nmeasures*nCopies,nobs]
    # for bootstrap resampling.
    # arr is a [nblocks][nwhatever] array
    nBlocks = int(np.floor(arr.shape[0]/(nCopies*blockSize)))
    cutArr = arr[:nBlocks*blockSize*nCopies,:] #notice we can't use a non complete block
    cutArr = np.reshape(cutArr,(nBlocks,blockSize*nCopies,arr.shape[1]))

    print(arr.shape)
    print(cutArr.shape)
    delArr = np.delete(cutArr,blockIndex,axis=0)
    print(delArr.shape)

    delArr = np.reshape(delArr,((nBlocks-1)*blockSize*nCopies,arr.shape[1]))
    print(delArr.shape)


    return delArr

def deleteBlock1D(arr,blockSize,blockIndex):
    # arr MUST be 1-dimensional [nmeasures]
    # for bootstrap resampling.
    # arr is a [nblocks][nwhatever] array
    nBlocks = int(np.floor(arr.shape[0]/blockSize))
    cutArr = arr[:nBlocks*blockSize]  #notice we can't use a non complete block
    cutArr = np.reshape(cutArr,(nBlocks,blockSize))

    delArr = np.delete(cutArr,blockIndex,axis=0)
    
    delArr = np.reshape(delArr,((nBlocks-1)*blockSize))

    return delArr






def power(data,col,n):
    # arr MUST be bi-dimensional [nmeasures,nobs]
    return data[:,col]**n

def mean(data,col):
    # arr MUST be bi-dimensional [nmeasures,nobs]
    return np.mean(data[:,col])

def variance(data,col):
    # arr MUST be bi-dimensional [nmeasures,nobs]
    return np.mean(data[:,col]**2) - mean(data,col)**2

def binder(data,col):
    # arr MUST be bi-dimensional [nmeasures,nobs]
    return np.mean(data[:,col]**4)/(np.mean(data[:,col]**2)**2)



# This function works on a list of observables
def meanerrBootstrapJackknifeBlocking(data,blockSize,resamples,observables):
    # arr MUST be bi-dimensional [nmeasures,nobs]

    obsMeanValues = []
    obsErrors = []
    obsResampledLists = []

    for jackKnifeObs in observables:
        obsMeanValues.append(jackKnifeObs(data,blockSize))
        obsResampledLists.append([])

    # error calculation with bootstrap

    for _ in range(resamples):
        reshuffledData = reshuffleBlock(data,blocksize)
        for jackKnifeObs,obsResampledList in zip(observables,obsResampledLists):
            obsResampledList.append(jackKnifeObs(reshuffledData,blockSize))

    
    for obsResampledList in obsResampledLists:
        obsErrors.append(np.std(obsResampledList),ddof = 1)
    

    return obsMeanValues, obsErrors 
 


##########################################################
###   SPECIFIC OBSERVABLES, WITH JACKKNIFE PROCEDURES  ###
##########################################################

#using powers

def suscJacknifeBlocking(data,blockSize,column):
    nBlocks = int(np.floor(data.shape[0]/blockSize))
    sizeCut = nBlocks*blockSize

    # from N-1 samples
    # NOTATION : '...sNm' stands for:
    # s : is plural, it's an array nBlocks long
    # Nm : N-1, it's the observable calculated on N-1 blocks (which is biased)
    meansNm = jackknifeBlockingLocalArrayPower(data[:sizeCut,:],blockSize,1,column)
    squaresNm = jackknifeBlockingLocalArrayPower(data[:sizeCut,:],blockSize,2,column)

    suscsNm = squaresNm - meansNm**2

    suscNm = np.mean(suscsNm)

    # from N samples
    mean = np.mean(data[:sizeCut,column])
    square = np.mean(data[:sizeCut,column]**2)

    suscN = square - mean**2
 
    # unbiased value
    susc = nBlocks * suscN - (nBlocks-1) * suscNm
    # jackknife esimation of error
    suscErrJNm = np.sqrt(nBlocks)*np.std(suscsNm, ddof = 1 )\

    return susc, suscErrJNm


def BinderJacknifeBlocking(data,blockSize,column):
    nBlocks = int(np.floor(data.shape[0]/blockSize))
    sizeCut = nBlocks*blockSize

    # from N-1 samples
    # NOTATION : '...sNm' stands for:
    # s : is plural, it's an array nBlocks long
    # Nm : N-1, it's the observable calculated on N-1 blocks (which is biased)
    squaresNm = jackknifeBlockingLocalArrayPower(data,blockSize,2,column)
    quarticsNm = jackknifeBlockingLocalArrayPower(data,blockSize,4,column)

    BindersNm = quarticsNm/squaresNm**2
    BinderNm = np.mean(BindersNm)

    # from N samples
    #mean = np.mean(data[:,column])
    square = np.mean(data[:sizeCut,column]**2)
    quartic = np.mean(data[:sizeCut,column]**4)

    BinderN = quartic/square**2
 
    # unbiased value
    Binder = nBlocks * BinderN - (nBlocks-1) * BinderNm
    # jackknife esimation of error
    BinderErrJNm = np.sqrt(nBlocks)*np.std(BindersNm, ddof = 1 )

    return Binder, BinderErrJNm



