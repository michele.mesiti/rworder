
# the following is based on 
# Newman, Barkema  - MonteCarlo Methods in Statistical Physics
# by Oxford University Press Inc., New York
# M. E. J. Newman and G. T. Barkema, 1999
# First published 1999
# Reprinted (with corrections) 2001
# chapter 8 - Analysing Monte Carlo data
# section 2 - the multiplt histogram method

import numpy as np
from sys import argv, stderr
import bootstrapblocking.jackknife_blocking as jb
import chooks as ch # for calculation involving logarithms
import time

import pickle

# See eq 8.37
# The denominators are of the kind
# e^{\beta E_i} * \sum e^{\ln n_j -\beta_j E_i - logZ_j  }
# j < nBetas
# i < nMeas

def multiRwDenominatorLogs(betas,ns,logZetas,beta,energies):
    # betas,ns,logZetas --> [nBetas]
    # energies ---> [nMeas] (the sum from ALL files)
    
    nMeas = energies.shape[0]
    nBetas = betas.shape[0]

    # 1D - only 'j' index 
    newLogs1D =  np.log(ns) - logZetas # [nBetas], must be replicated [nMeas] times
    newLogs2D = np.outer(np.ones_like(energies),newLogs1D) # [nMeas,nBetas] 
    newLogs2D -= np.outer(energies,betas - beta ) # [nMeas,nBetas]
    
    res = np.zeros_like(energies) # [nMeas]

    ch.arrSumLog2D(newLogs2D,nMeas,nBetas,res) # result is [nMeas] long

    return res

def calcWeigths(betas,ns,logZetas,beta,energies):
    denominatorLogs = multiRwDenominatorLogs(betas,ns,logZetas,beta,energies)#[nMeas]
    LogZeta = ch.arrSumLog(-denominatorLogs,denominatorLogs.shape[0]) # 1
    weights = np.exp(-denominatorLogs-LogZeta) #[nMeas]  # normalized!!
    return weights

def densityOfStatesLogHistogram(betas,ns,logZetas,allEnergies):
    
    denominatorLogs = multiRwDenominatorLogs(betas,ns,logZetas,0,allEnergies)
    nBins = int(np.sqrt(allEnergies.size))
    
    eMin = np.min(allEnergies)
    eMax = np.max(allEnergies)
    binWidth = (eMax-eMin)/nBins

    binLogDensitiesOfStates = []
    binCenters = []
    numerosities = []
    for i in range(nBins):
        selectCondition =np.logical_and(allEnergies > eMin+i*binWidth,\
                allEnergies < eMin +(i+1)*binWidth)
        weightsToSum = np.ascontiguousarray(-denominatorLogs[selectCondition])

        if weightsToSum.size > 0:
            binLogDensityOfStates = ch.arrSumLog(weightsToSum,weightsToSum.size)
            binCenter = eMin + (i+0.5)*binWidth
            binLogDensitiesOfStates.append(binLogDensityOfStates)
            binCenters.append(binCenter)
            numerosities.append(weightsToSum.size)

    binLogDensitiesOfStates = np.array(binLogDensitiesOfStates)
    binCenters = np.array(binCenters) 
    numerosities = np.array(numerosities)

    return binCenters, binLogDensitiesOfStates, numerosities


def calcLogZetas(betas,energyDatas,logZetas,tolerance = 1e-7):


    ns = np.array([energyData.shape[0] for energyData in energyDatas ])
    if logZetas is None:
        logZetas = np.zeros_like(ns)

    allEnergies = np.hstack(energyDatas)

    variation = 1
    while variation > tolerance:
        variations = []
        newLogZetas = []
        for beta,logZeta in zip(betas,logZetas):
            denominatorLogs = multiRwDenominatorLogs(betas,ns,logZetas,beta,allEnergies)
            newLogZeta = ch.arrSumLog(-denominatorLogs,denominatorLogs.shape[0])
            variations.append(newLogZeta-logZeta)
            newLogZetas.append(newLogZeta)
            #logZeta = newlogZeta
        logZetas = np.array(newLogZetas) - newLogZetas[0]
        variation = np.sum(np.array(variations)**2)
        print("LogZetas: ", logZetas, " Variation: " , variation )

    return logZetas 


# this function DOES NOT FIX BIAS (unless obsFun does it in some way).
def calcObsBootstrap(betas,energyDatas,exactLogZetas,obsFuns,otherObsFunArgs,\
        obsDatas,blockSizes,betaMin,betaMax,betaStep,bootstrapSteps,tolerance=1e-7,\
        nCopies=1):
    #returns a list of array of values and an array of errors [nPoints],
    # which is [nFuns] longs
    # NOTICE : nPoints is not nBetas
    # all obsDatas must be 2D
    # all energyDatas must be 1D

    betaPoints = np.arange(betaMin,betaMax,betaStep)

    nPoints = len(betaPoints)
    allObsValuesPerBeta = dict()  # [nPoints][nFuns]
    ns = np.array([energyData.shape[0] for energyData in energyDatas ])
    stderr.write("\nCalculating observables for BetaPoints...\n")
    for betaPoint in betaPoints:
        stderr.write("%.5f \r" % betaPoint)
        stderr.flush()
        allEnergies = np.hstack(energyDatas)
        allObss = np.vstack(obsDatas)
        weights = calcWeigths(betas,ns,exactLogZetas,betaPoint,allEnergies)
        obsReturnValues = dict()  # [nFuns]
        for obsFun in obsFuns:
            obsReturnValues[obsFun.__name__] = obsFun(allObss,weights,otherObsFunArgs)
        allObsValuesPerBeta[betaPoint] = obsReturnValues
    # average values calculated

    funMeans = dict()  # [nFuns][nBetas]
    for obsFun in obsFuns:
        funMeans[obsFun.__name__] = dict()
        for betaPoint in betaPoints:
            funMeans[obsFun.__name__][betaPoint] = allObsValuesPerBeta[betaPoint][obsFun.__name__]

    # now, bootstrap


    allObsValuesPerBetaBootstraps = []   # [nBoot][nPoints][nFuns]
    startTime = time.time()
    for i in range(bootstrapSteps):
        stderr.write("Bootstrapping (%d/%d)..\n" % (i+1,bootstrapSteps))
        stderr.flush()
        if i ==  0:
            stderr.write('\n')
        else:
            totExpectedTime = (endTime-startTime)/i*bootstrapSteps
            stderr.write("completing in {:d}s of total expected {:d}s\n"\
                    .format(int(totExpectedTime-(endTime-startTime)),\
                    int(totExpectedTime)))
        
        #reshuffling
        reshuffledEnergyDatas = []
        reshuffledObsDatas = []
        stderr.write("Reshuffling...\n")
        stderr.flush()
        for blockSize,energyData,obsData in zip(blockSizes,energyDatas,obsDatas):
            reshuffledEnergyData,indexes = jb.reshuffleBlock1D(energyData,blockSize)
            reshuffledEnergyDatas.append(reshuffledEnergyData) 
            reshuffledObsData,indexes = jb.reshuffleBlock2D(obsData,blockSize,\
                    indexes,nCopies)
            reshuffledObsDatas.append(reshuffledObsData) 
        #reshuffled
        stderr.write("Recalculating zetas with {} tolerance...\n".format(tolerance))
        exactLogZetasRecalculated = calcLogZetas(betas,reshuffledEnergyDatas,\
                exactLogZetas,tolerance)

        reshuffledAllEnergies = np.hstack(reshuffledEnergyDatas)
        reshuffledAllObs = np.vstack(reshuffledObsDatas)


        stderr.write("\nCalculating observables for BetaPoints...\n")
        allObsValuesPerBeta = dict()  # [nPoints][nFuns]
        for betaPoint in betaPoints:
            stderr.write("%.5f \r" % betaPoint)
            stderr.flush()
            weights = calcWeigths(betas,ns,exactLogZetasRecalculated,betaPoint,\
                    reshuffledAllEnergies)
            obsReturnValues = dict()  # [nFuns]
            for obsFun in obsFuns:
                obsReturnValues[obsFun.__name__] = obsFun(reshuffledAllObs,weights,otherObsFunArgs)
            allObsValuesPerBeta[betaPoint] = obsReturnValues
        
        allObsValuesPerBetaBootstraps.append(allObsValuesPerBeta)
        stderr.write("\n")
        stderr.flush()
        endTime = time.time()

    
    funMeanErrBootstrap = dict()  # [nFuns]
    for obsFun in obsFuns:
        funMeanErrBootstrap[obsFun.__name__] = dict() # [nBetas]
        for betaPoint in betaPoints:
            valueList =  [ element[betaPoint][obsFun.__name__] for element in\
                     allObsValuesPerBetaBootstraps]
            valueArr = np.array(valueList)
            funMeanErrBootstrap[obsFun.__name__][betaPoint] = \
                    np.mean(valueArr), np.std(valueArr, ddof=1)

    return betaPoints, funMeans,funMeanErrBootstrap


# this function DOES NOT FIX BIAS (unless obsFun does it in some way).
def calcObsJK(betas,energyDatas,exactLogZetas,obsFuns,otherObsFunArgs,\
        obsDatas,blockSizes,betaMin,betaMax,betaStep,tolerance=1e-7,\
        nCopies=1,ichunk=None,nchunks=None):
    #returns a list of array of values and an array of errors [nPoints],
    # which is [nFuns] longs
    # NOTICE : nPoints is not nBetas
    # all obsDatas must be 2D
    # all energyDatas must be 1D

    betaPoints = np.arange(betaMin,betaMax,betaStep)

    nPoints = len(betaPoints)
    allObsValuesPerBeta = dict()  # [nPoints][nFuns]
    ns = np.array([energyData.shape[0] for energyData in energyDatas ])
    stderr.write("\nCalculating observables for BetaPoints...\n")
    for betaPoint in betaPoints:
        stderr.write("%.5f \r" % betaPoint)
        stderr.flush()
        allEnergies = np.hstack(energyDatas)
        allObss = np.vstack(obsDatas)
        weights = calcWeigths(betas,ns,exactLogZetas,betaPoint,allEnergies)
        obsReturnValues = dict()  # [nFuns]
        for obsFun in obsFuns:
            obsReturnValues[obsFun.__name__] = obsFun(allObss,weights,otherObsFunArgs)
        allObsValuesPerBeta[betaPoint] = obsReturnValues
    # average values calculated

    funMeans = dict()  # [nFuns][nBetas]
    for obsFun in obsFuns:
        funMeans[obsFun.__name__] = dict()
        for betaPoint in betaPoints:
            funMeans[obsFun.__name__][betaPoint] = allObsValuesPerBeta[betaPoint][obsFun.__name__]

    # now, jackknife

    #cropping all arrays
    obsDatasCropped ,energyDatasCropped = [], [] 
    totBlocks = 0
    for blockSize, obsData, energyData in zip(blockSizes, obsDatas, energyDatas):
        nBlocks = int(np.floor(energyData.shape[0]/blockSize))
        totBlocks += nBlocks
        nMeasCropped = blockSize*nBlocks
        energyDataCropped = energyData[:nMeasCropped]
        obsDataCropped = obsData[:nMeasCropped*nCopies,:]
        energyDatasCropped.append(energyDataCropped)
        obsDatasCropped.append(obsDataCropped)


    allObsValuesPerBetaJackKnifes = []   # [nBoot][nPoints][nFuns]
    iBlocks = 0

    chunkMode = False
    if ichunk is not None and nchunks is not None:
        chunkMode = True
        chunkSize = np.ceil(totBlocks/nchunks)
        iBlockStart = chunkSize * ichunk
        iBlockEnd = min(iBlockStart + chunkSize, totBlocks)
        stderr.write("chunkMode True, starting from block {} and stopping at block {}\n".\
                format(iBlockStart+1,iBlockEnd))
    else:
        iBlockStart = 0
        iBlockEnd = totBlocks
 
    blocksToDo = iBlockEnd - iBlockStart


    startTime = time.time()
    for iBeta,el in \
            enumerate(zip(obsDatasCropped,energyDatasCropped,blockSizes)):
        obsData,energyData,blockSize = el
        nBlocks = int(np.floor(energyData.shape[0]/blockSize))
        for blockIndex in range(nBlocks):
            iBlocks += 1
            #stderr.write("On block (%d-%d)/(%d-%d) [tot %d/%d ]..\n" % \
            #            (iBeta+1,blockIndex+1,len(obsDatas),nBlocks, iBlocks, totBlocks))

            if chunkMode:
                doThisIteration = ( iBlocks > iBlockStart and iBlocks <= iBlockEnd )

            if not chunkMode or (chunkMode and doThisIteration ):
                stderr.write("JackKnifing (%d-%d)/(%d-%d) [tot %d/%d ].." % \
                        (iBeta+1,blockIndex+1,len(obsDatas),nBlocks, iBlocks, totBlocks))
                if iBlocks == iBlockStart+1:
                    stderr.write('\n')
                else:
                    totExpectedTime = (endTime-startTime)/(iBlocks-iBlockStart-1)*blocksToDo
                    stderr.write("completing in {:d}s of total expected {:d}s\n"\
                        .format(int(totExpectedTime-(endTime-startTime)),\
                        int(totExpectedTime)))
                    stderr.flush()
                #jackKnifing
                jackKnifedEnergyDatas = energyDatasCropped[:iBeta]
                jackKnifedObsDatas = obsDatasCropped[:iBeta]
                stderr.flush()
                # jackknifing selected beta and selected block
                jackKnifedEnergyData = jb.deleteBlock1D(energyData,blockSize,blockIndex)
                jackKnifedEnergyDatas.append(jackKnifedEnergyData) 
                jackKnifedObsData = jb.deleteBlock2D(obsData,blockSize,blockIndex,nCopies)
                jackKnifedObsDatas.append(jackKnifedObsData) 
                #jackKnifing
                jackKnifedEnergyDatas += energyDatasCropped[iBeta+1:]
                jackKnifedObsDatas += obsDatasCropped[iBeta+1:]
                stderr.flush()
                
        
                #jackknifed
                stderr.write("Recalculating zetas with {} tolerance...\n".format(tolerance))
                exactLogZetasRecalculated = calcLogZetas(betas,jackKnifedEnergyDatas,\
                        exactLogZetas,tolerance)
        
                jackKnifedAllEnergies = np.hstack(jackKnifedEnergyDatas)
                jackKnifedAllObs = np.vstack(jackKnifedObsDatas)
        
        
                stderr.write("\nCalculating observables for BetaPoints...\n")
                allObsValuesPerBeta = dict()  # [nPoints][nFuns]
                for betaPoint in betaPoints:
                    stderr.write("%.5f \r" % betaPoint)
                    stderr.flush()
                    weights = calcWeigths(betas,ns,exactLogZetasRecalculated,betaPoint,\
                            jackKnifedAllEnergies)
                    obsReturnValues = dict()  # [nFuns]
                    for obsFun in obsFuns:

                        obsReturnValues[obsFun.__name__] = obsFun(jackKnifedAllObs,weights,otherObsFunArgs)
                    allObsValuesPerBeta[betaPoint] = obsReturnValues
                
                allObsValuesPerBetaJackKnifes.append(allObsValuesPerBeta)
                stderr.write("\n")
                stderr.flush()
                endTime = time.time()
            # end if chunkMode is false of doThisIterations is True
        # end for blockIndex
    # end for betas

    if chunkMode:
        stderr.write("Creating Pickle dump...\n")
        PickleDump = pickle.dumps(allObsValuesPerBetaJackKnifes)
    else:
        funMeanErrJackKnife = dict()  # [nFuns]
        for obsFun in obsFuns:
            funMeanErrJackKnife[obsFun.__name__] = dict() # [nBetas]
            for betaPoint in betaPoints:
                valueList =  [ element[betaPoint][obsFun.__name__] for element in\
                         allObsValuesPerBetaJackKnifes]
                valueArr = np.array(valueList)
                funMeanErrJackKnife[obsFun.__name__][betaPoint] = \
                        np.mean(valueArr), np.std(valueArr, ddof=1)*np.sqrt(totBlocks) # YOU KNOW WHY

    if chunkMode:
        return PickleDump 
    else :
        return betaPoints, funMeans,funMeanErrJackKnife





