#!/usr/bin/env python3
from sys import argv,exit,stdout, stderr
import re
import renormalization as ren
import numpy as np
from os import path

inputFileName = argv[1]
stdout.write("Reading {}\n".format(inputFileName))

inputFile = open(inputFileName)
lines = inputFile.readlines()
# removing comments
lines = [ line[:line.find('#')] for  line in lines ] 

finiteAndNullTFileNames = [ tuple(line.split()[:2]) for line in lines]
finiteAndNullTFileNames = [ t for t in finiteAndNullTFileNames if len(t) == 2 ]

for finiteTFileName, nullTFileName in finiteAndNullTFileNames:
    if nullTFileName not in ['-','none']:
        finiteTTable = np.loadtxt(finiteTFileName)
        nullTTable = np.loadtxt(nullTFileName)
        
        betaMin = np.min(nullTTable[:,ren.betaIndex])
        betaMax = np.max(nullTTable[:,ren.betaIndex])
        
        inclusionCondition = betaMin <= finiteTTable[:,ren.betaIndex] 
        inclusionCondition = np.logical_and(\
                           finiteTTable[:,ren.betaIndex] <= betaMax,inclusionCondition)
        finiteTTable = finiteTTable[inclusionCondition,:]
        
        renormalizedCond = ren.renormalizeBootstrap(nullTTable,finiteTTable,'multiplicative',\
                'spline',ren.condensateIndex);
        # notice: this does not fix multiplicative renormalization in chiral susc
        renormalizedSusc = ren.renormalizeBootstrap(nullTTable,finiteTTable,'additive',\
                'linear',ren.suscIndex);
        
        renOss = np.hstack((renormalizedCond,renormalizedSusc[:,1:]))
        renOssPrefix = 'renChir_'
        outRenFileName = renOssPrefix + path.basename(finiteTFileName )
        header = 'beta , renCond, err, renSusc(NO MULTIPLICATIVE RENORMALIZATION), err '
        stdout.write("Writing {}\n".format(outRenFileName ))
        np.savetxt( outRenFileName, renOss)
    else:
        stdout.write("File at t=0 not specified for {}\n".format(finiteTFileName))
        finiteTTable = np.loadtxt(finiteTFileName)
       
        NONrenormalizedCond = finiteTTable[:, [ren.betaIndex,ren.condensateIndex,\
                ren.condensateIndex+1]]
        NONrenormalizedSusc = finiteTTable[:, [ ren.betaIndex,ren.suscIndex,\
                ren.suscIndex+1]]

     
        NONrenOss = np.hstack((NONrenormalizedCond,NONrenormalizedSusc[:,1:]))
        NONrenOssPrefix = 'Chir_'
        outNONRenFileName = NONrenOssPrefix + path.basename(finiteTFileName )
        header = 'beta , Cond (NONRENORMALIZED), err, Susc (NONRENORMALIZED), err'
        stdout.write("Writing {}\n".format(outNONRenFileName ))
        np.savetxt( outNONRenFileName, NONrenOss, header = header)






