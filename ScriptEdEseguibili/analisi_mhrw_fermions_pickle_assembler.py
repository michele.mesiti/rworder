#!/usr/bin/env python3

import numpy as np
from sys import argv,stdout,stderr,exit
import pickle
import osservabili_rw_mhrw as orw



betaPoints = None

obsFuns = orw.fermObsMHRW
funNames = [ obsFun.__name__ for obsFun in orw.fermObsMHRW ] 

valuesLists = dict()
totBlocks = 0

# creating lists

# loading data, concatenating all the lists
for fileName in argv[2:]:

    stdout.write("Reading {} \n".format(fileName))

    f = open(fileName,'rb')
    allObsValuesPerBetaJackKnifes = pickle.load(f)
    f.close()

    for obsFun in obsFuns:
        print( obsFun.__name__)
        if obsFun.__name__ not in valuesLists:
            valuesLists[obsFun.__name__] = dict() # [nBetas]
        if betaPoints is None:
            betaPoints = allObsValuesPerBetaJackKnifes[0].keys()
            print(allObsValuesPerBetaJackKnifes[0].keys())

        for betaPoint in betaPoints:
            if betaPoint not in valuesLists[obsFun.__name__]:
                valuesLists[obsFun.__name__][betaPoint] = []
            valueList =  [ element[betaPoint][obsFun.__name__] for element in\
                     allObsValuesPerBetaJackKnifes]
            valuesLists[obsFun.__name__][betaPoint] += valueList




funMeanErrJackKnife = dict()
for obsFun in obsFuns:
    funMeanErrJackKnife[obsFun.__name__] = dict() # [nBetas]
    for betaPoint in betaPoints:
        totBlocksCheck = len(valuesLists[obsFun.__name__][betaPoint])
        valuesLists[obsFun.__name__][betaPoint] = np.array(valuesLists[obsFun.__name__][betaPoint])
        if totBlocks == 0:
            totBlocks = totBlocksCheck
        elif totBlocks != totBlocksCheck:
            stdout.write("Error: totBlocksCheck {} vs totBlocks {} ( observable:{} beta:{})\n"\
                    .format(totBlocksCheck, totBlocks, obsFun.__name__, betaPoint ))
        stdout.write("TotBlocksCheck {} vs totBlocks {} ( observable:{} beta:{})\n"\
                .format(totBlocksCheck, totBlocks, obsFun.__name__, betaPoint ))


        funMeanErrJackKnife[obsFun.__name__][betaPoint] = \
                    np.mean(valuesLists[obsFun.__name__][betaPoint]),\
                    np.std(valuesLists[obsFun.__name__][betaPoint], ddof=1)\
                    *np.sqrt(totBlocks) # YOU KNOW WHY



print(betaPoints)

outfileName = argv[1] + '.MHRWout.fermion.dat'
stdout.write("Writing file {}\n".format(outfileName))
outfile = open(outfileName, 'w')

# writing HEADER
outfile.write('#beta')
stdout.write('#beta')
for funName in funNames:
    outfile.write("\t{}\t(err)".format(funName))
    stdout.write("\t{}\t(err)". format(funName))
outfile.write('\n')
stdout.write('\n')

for betaPoint in betaPoints:
    stdout.write("{:1.5f}".format(betaPoint))
    outfile.write("{:1.5f}".format(betaPoint)) 
    for funName in funNames:
        mean = funMeanErrJackKnife[funName][betaPoint][0]
        err = funMeanErrJackKnife[funName][betaPoint][1]
        stdout.write("  {:.15e}  {:.15e}".format(mean,err))
        outfile.write("  {:.15e}  {:.15e}".format(mean,err))

    stdout.write("\n")
    outfile.write("\n")

outfile.close()

           










