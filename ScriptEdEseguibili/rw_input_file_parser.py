#!/usr/bin/env python3

import numpy as np
from collections import namedtuple
import itertools as it
from sys import exit,stderr,stdout

header = '#beta	therm	block	nCopies	logZetasMHRWstart	runId	volume	LASTGAUGEFILE 	LASTFERMFILE	MODE	CUTMODE\n'


class runRecord:
    def __init__(self,therm,runIndice,gaugeFileName,fermionicFileName,cutMode):
        self.therm             = therm            
        self.runIndice         = runIndice        
        self.gaugeFileName     = gaugeFileName    
        self.fermionicFileName = fermionicFileName
        self.cutMode           = cutMode          

class betaRecord:
    def __init__(self,beta,block,nFermCopie,logZetaMHRWstart,volume,mode,runRecords):

        self.beta             =  beta
        self.block            =  block            
        self.nFermCopie       =  nFermCopie            
        self.logZetaMHRWstart =  logZetaMHRWstart            
        self.volume           =  volume            
        self.mode             =  mode
        self.runs             =  runRecords            



class analisiSetting:

    def __init__(self, inputFileName):
        self.parseInputFile(inputFileName)

    def parseInputFile(self,inputFileName):

        table = np.genfromtxt(inputFileName,dtype=None)
    
        self.betas             = [ float(lineRead[0]) for lineRead in table ]
        self.therms            = [ int(  lineRead[1]) for lineRead in table ]
        self.blocks            = [ int(  lineRead[2]) for lineRead in table ]
        self.nFermCopies       = [ int(  lineRead[3]) for lineRead in table ]
        self.logZetasMHRWstart = [ float(lineRead[4]) for lineRead in table ]
        self.runIndices        = [ int(  lineRead[5]) for lineRead in table ]
        self.volumes           = [ int(  lineRead[6]) for lineRead in table ]
        self.gaugeFileNames    =       [ lineRead[7].decode('utf-8') for lineRead in table ]
        self.fermionicFileNames =      [ lineRead[8].decode('utf-8') for lineRead in table ]
        self.modes               =     [ lineRead[9].decode('utf-8') for lineRead in table ] 
        self.cutModes            =    [ lineRead[10].decode('utf-8') for lineRead in table ]

        infoTuple = namedtuple("betaRecord",['beta','block','nFermCopie','volume','mode','logZetaMHRWstart'])
        runRecord = namedtuple("runRecord", ['therm','runIndice','gaugeFileName','fermionicFileName','cutMode'])
        betaTuple = namedtuple("betaTuple", ["info","runRecords"])
                
        # all the different betas and volumes and the like
        runGroupInfos = []
        for beta,block,nFermCopie,volume,mode,logZetaMHRWstart\
                in zip(self.betas,self.blocks,self.nFermCopies,\
                self.volumes,self.modes,self.logZetasMHRWstart):
            runGroupInfos.append(infoTuple(beta,block,nFermCopie,volume,mode,logZetaMHRWstart))

        runGroupInfos = set(runGroupInfos)
        
        self.runListPerGroup = dict()
        for runGroupInfo in runGroupInfos:
            self.runListPerGroup[runGroupInfo] = []

        # check
        for rga,rgb in it.product(runGroupInfos,runGroupInfos):
            if rga.beta == rgb.beta and rga != rgb:
                stderr.write("Problem: found two sets of run at the same beta having different properties! beta = {}\n".format(rga.beta))
                print(rga)
                print(rgb)
                exit(1)

        
        for      beta ,      therm ,    block ,     nFermCopie ,     logZetaMHRWstart,\
             runIndice ,     volume,      gaugeFileName ,     fermionicFileName ,\
             mode ,     cutMode     in\
        zip(self.betas,self.therms,self.blocks,self.nFermCopies,self.logZetasMHRWstart,\
        self.runIndices,self.volumes,self.gaugeFileNames,self.fermionicFileNames,\
        self.modes,self.cutModes):
            runGroupInfo = infoTuple(beta,block,nFermCopie,volume,mode,logZetaMHRWstart)
            run = runRecord(therm,runIndice,gaugeFileName,fermionicFileName,cutMode)
            self.runListPerGroup[runGroupInfo].append(run)

        self.betaInfo = dict()

        for runGroupInfo in self.runListPerGroup:
            self.betaInfo[runGroupInfo.beta] = \
                    betaTuple(info = runGroupInfo, runRecords = self.runListPerGroup[runGroupInfo] )




    def writeInputFile(self,outputFileName,logZetasDict):
        stdout.write("Writing {}\n".format(outputFileName))
        outFile = open(outputFileName,'w')
        outFile.write(header)
        for beta,therm,block,nFermCopie,runIndice,volume,gaugeFileName,fermionicFileName,mode,\
                cutMode in zip(self.betas,self.therms,self.blocks,self.nFermCopies,\
                self.runIndices,self.volumes,self.gaugeFileNames,self.fermionicFileNames,\
                self.modes,self.cutModes):
           if beta in logZetasDict:
               logZetaMHRWstart = logZetasDict[beta]
           else:
               logZetaMHRWstart = 0
           outFile.write("{}\t".format(beta            )) 
           outFile.write("{}\t".format(therm           )) 
           outFile.write("{}\t".format(block           )) 
           outFile.write("{}\t".format(nFermCopie      )) 
           outFile.write("{}\t".format(logZetaMHRWstart)) 
           outFile.write("{}\t".format(runIndice        ))
           outFile.write("{}\t".format(volume           ))
           outFile.write("{}\t".format(gaugeFileName    ))
           outFile.write("{}\t".format(fermionicFileName))
           outFile.write("{}\t".format(mode            )) 
           outFile.write("{}\t".format(cutMode         )) 
           outFile.write("\n")
        outFile.close()
