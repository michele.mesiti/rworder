#!/usr/bin/env python3

import numpy as np
import rw_input_file_parser as parser
from sys import argv,exit,stdout, stderr
#from matplotlib import pyplot as plt


import osservabili_rw as orw
from gauge_and_fermion_correction import *

if "TC" in argv:
    argv.remove('TC')
    thesisCorrMode = True
    fermObs = orw.fermObs_TC
else:
    thesisCorrMode = False
    fermObs = orw.fermObs


stdout.write("Reading {}\n".format(argv[1]))
ifp = parser.analisiSetting(argv[1])

outfileName = argv[1] + '.out.dat'
stdout.write("Writing file {}\n".format(outfileName))
outfile = open(outfileName, 'w')

# writing HEADER
outfile.write('#beta')
stdout.write('#beta')
for observable in fermObs + orw.gaugeObs:
    outfile.write("\t{}\t(err)".format(observable.__name__))
    stdout.write("\t{}\t(err)".format(observable.__name__))
outfile.write('\n')
stdout.write('\n')


for beta in sorted(ifp.betaInfo.keys()):
    runGroupInfo = ifp.betaInfo[beta].info
    runRecords   = ifp.betaInfo[beta].runRecords

    gaugeDatas = []
    fermionicDatas = []
    for runInfo in runRecords:

        fermionicSaneFileName = runInfo.fermionicFileName + correctionSuffix
        stdout.write("Reading {}\n".format(fermionicSaneFileName))
        fermionicDataSane = np.loadtxt(fermionicSaneFileName)
        if (fermionicDataSane.size != 0):
            fermionicDatas.append(fermionicDataSane)
       
        # not using the totally cleaned gauge file, only the one 
        # where the problems in the gauge file are removed
        gaugeSaneFileName = runInfo.gaugeFileName + minimalFixSuffix 
        stdout.write("Reading {}\n".format(gaugeSaneFileName    ))
        gaugeDataSane = np.loadtxt(gaugeSaneFileName)
        if (gaugeDataSane.size != 0):
            gaugeDatas.append(gaugeDataSane)
        
        stderr.write("beta: {}, mode: {}, thermalization traj {}\n".format(beta, runGroupInfo.mode, runInfo.therm))
    
    if len(gaugeDatas) == 0 or len(fermionicDatas) == 0:
        stdout.write("No valid data found for beta = {}!".format(beta))
        stdout.write("gaugedatas : {}  fermionicDatas {}".format(len(gaugeDatas),len(fermionicDatas)))
    else:
        gaugeData = np.vstack(gaugeDatas)
        fermionicData = np.vstack(fermionicDatas)

        # SELECTING  FERMIONIC FILE MODE
        idx = orw.indexDictionary(runGroupInfo.mode)
        orw.verbose = False

        stdout.write("{:1.5f}".format(beta))
        outfile.write("{:1.5f}".format(beta))
        
        stderr.write("Calculating fermionic observables...\n")

        measEvery = 1
        if runGroupInfo.mode == 'mode2.0_28':
            stdout.write("\nNOTICE: with mode2.0_28, expected measEvery = 4 (for fermions).\n")
            stdout.write("        Rescaling block size accordingly for fermion observables.\n")
            measEvery = 4

         
        for observable in fermObs:
            mean,err = observable(fermionicData, int(np.floor(runGroupInfo.block/measEvery)),\
                    runGroupInfo.nFermCopie, runGroupInfo.volume, idx)
            stdout.write("  {:.15e}  {:.15e}".format(mean,err))
            outfile.write("  {:.15e}  {:.15e}".format(mean,err))
        
        stderr.write("Calculating gauge observables...\n")
        for observable in orw.gaugeObs:
            mean,err = observable(gaugeData, runGroupInfo.block, idx)
            stdout.write("  {:.15e}  {:.15e}".format(mean,err))
            outfile.write("  {:.15e}  {:.15e}".format(mean,err))
        stdout.write("\n")
        outfile.write("\n")
        stderr.write("\n")



    






