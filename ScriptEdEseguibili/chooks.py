#!/usr/bin/python3

import ctypes
from numpy.ctypeslib import ndpointer
lib = ctypes.cdll.LoadLibrary("/home/mesiti/PythonScripts/chooks/chooks.so")

# see chooks.c
arrSumLog = lib.logSumFromLogArr
arrSumLog.restype = ctypes.c_double
arrSumLog.argtypes = [ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),ctypes.c_int]

arrSumLog2D = lib.logSumFromLog2DArr
arrSumLog2D.restype = None
arrSumLog2D.argtypes = [ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),ctypes.c_int,\
        ctypes.c_int, ndpointer(ctypes.c_double, flags="C_CONTIGUOUS")]

lineSumNNmProd = lib.lineSumNNmProd
lineSumNNmProd.restype = None
lineSumNNmProd.argtypes = [ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),ctypes.c_int,\
        ctypes.c_int, ndpointer(ctypes.c_double, flags="C_CONTIGUOUS") ]


