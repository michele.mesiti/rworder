#!/usr/bin/env python3

import numpy as np
from sys import argv,stdout,stderr,exit
import pickle
import osservabili_rw_mhrw as orw

import analisi_mhrw_pickle_assembler as ass

whereDataIs = '../RawData/MHRW_results/'
stdout.write("Reading {}\n".format(argv[1]))
f = open(argv[1])
lines = f.readlines()
f.close()

# removing comments
lines = [ line[:line.find('#')] for  line in lines ] 
mhrwSettings = [ tuple(line.split()[:5]) for line in lines ] 
mhrwSettings = [ t for t in mhrwSettings if len(t) == 5 ] 

#assembling
rootFileNameProblems = []
for rootFileName, bmin, bmax, fchunks, gchunks in mhrwSettings:
    for mode,nchunks in [ ('fermion',fchunks), ('gauge',gchunks)]:
        if mode == 'gauge':
            pref = 'gauge_'
        else :
            pref = ''
        allFileNames = [ whereDataIs + pref + rootFileName + 'chunk{}of{}.PICKLE'.\
           format(i,nchunks) for i in range(int(nchunks)) ]
        try:
            ass.doall(mode, rootFileName, allFileNames)
        except Exception as e:
            print(e)
            rootFileNameProblems.append(rootFileName)

print("\n\nPROBLEMS FOUND: {} OUT OF {}".format(len(rootFileNameProblems), len(mhrwSettings)))

for e in rootFileNameProblems:
    print(e)
           




    











