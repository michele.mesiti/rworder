import numpy as np
from sys import exit
from scipy.optimize import leastsq
from scipy import interpolate
import fastfit
import osservabili_rw as orw
#import osservabili_rw_mhrw as orw
# this library should work also on mhrw fermion observables


fNames = [ f.__name__ for f in orw.fermObs ] 

# fermion observables are at the beginning of the file anyway
# first is beta
betaIndex = 0
condensateIndex = 1+2*fNames.index( 'renLightCond1' ) 
suscIndex =       1+2*fNames.index( 'ChiralSuscLight' ) 

# multiplicative divergences in chiral susceptibility 
# are cured by mutiplying by mass
# unfortunately, data is missing for beta < 3.45
# this means we cannot perform multiplicative 
# renormalization of chiral susceptibility
# for T=4 lattices.
# Anyway, to study the order of the transition this should not be 
# a problem.
a_beta_ml = [\
[0.2824,3.45,1.57e-1],\
[0.2173,3.55,1.02e-1],\
[0.1535,3.67,6.39e-2],\
[0.1249,3.75,5.03e-2],\
[0.0989,3.85,3.94e-2]\
]

def div(a,b):
    return a/b

def sott(a,b):
    return a-b

def renormalize(xm,ym,xs,ys,dys,renmode, interpMode):
    # dys used only in chi squared for fit 

    if renmode == 'multiplicative':
        f = div
    elif renmode == 'additive':
        f = sott
    else:
        print("Renormalization mode {} not valid.".format(renmode))
        exit(1)

    # calculation of interpolated 'sottraendo'
    if interpMode == 'linear':
        (a,b), chisquare, ndof = fastfit.linfit(xs,ys,dys)
        #print("Linear interpolation. Chisq/Ndof = {}/{}".format(chisquare,ndof))
        interpFunction = lambda x : a*x+b
    if interpMode == 'spline':
        tck = interpolate.splrep(xs,ys,k = 1)
        interpFunction = lambda x : interpolate.splev(x,tck)

    return f(ym,interpFunction(xm))


def renormalizeBootstrap(tableNullT,tableFiniteT,renmode,interpMode,ossIndex,bootStrap=1000):

    xm =  tableFiniteT[:,betaIndex]
    ym =  tableFiniteT[:,ossIndex]
    dym = tableFiniteT[:,ossIndex+1]

    xs =  tableNullT[:,betaIndex]
    ys =  tableNullT[:,ossIndex]
    dys = tableNullT[:,ossIndex+1]

    yren = []

    for _ in range(bootStrap):
        ymt = np.random.normal(ym,dym)
        yst = np.random.normal(ys,dys)
        yren.append(renormalize(xm,ymt,xs,yst,dys,renmode,interpMode))

    yren = np.vstack(yren)
    myren = np.mean(yren,axis = 0)
    dyren = np.std(yren,axis = 0,ddof=1)
    
    return np.vstack((xm,myren,dyren)).T






