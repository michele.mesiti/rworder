#!/usr/bin/env python3

import numpy as np
from sys import argv,stdout


inputFileName = argv[1]
stdout.write("Reading {}\n".format(inputFileName))

inputFile = open(argv[1])
lines = inputFile.readlines()
# removing comments
lines = [ line[:line.find('#')] for  line in lines ] 

LandFileNames = [ tuple(line.split()[:2]) for line in lines]
LandFileNames = [ t for t in LandFileNames if len(t) == 2 ]

dataPoints = []
for L,fileName in LandFileNames:
    L = float(L)
    suscData = np.loadtxt(fileName)
    suscData = suscData[:,[0,3,4]]
    suscArgMax = np.argmax(suscData[:,1])
    suscMax = suscData[suscArgMax,1] 
    suscMaxErr = suscData[suscArgMax,2] 
    dataPoint = float(L),suscMax,suscMaxErr
    dataPoints.append(dataPoint)

dataPoints = np.array(dataPoints)

header = 'L, suscMax, err '
outFileName = inputFileName + '.out.dat'
stdout.write("Writing {}\n".format(outFileName))

np.savetxt(outFileName, dataPoints, header = header)



