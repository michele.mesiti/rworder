#!/bin/bash
#MIRROR='/home/users/mesiti/RWANALYSIS/RawData'
MIRROR='grid3:/gpfs/ddn/teorici/PIRACOL/RWANALISI2017/RawData'
#MIRROR='grid3:/home/users/mesiti/RWANALYSIS/RawData'
#MIRROR='sf:/home/users/mesiti/RWANALYSIS/RawData'
#MIRROR='coka:/home/mmesiti/RWANALYSIS/RawData'
#MIRROR='fe:/home/mmesiti/RWANALYSIS/RawData'

ssh grid3 "cd /gpfs/ddn/teorici/PIRACOL/RWANALISI2017/RawData; ./synchronize.sh "
echo "T=0 runs"
rsync -av  --include '*/' --include-from nuovi_run_michele_t0.rsync --exclude '*' --prune-empty-dirs $MIRROR/run_t0  ./
echo "T=0 runs, m=0.00075"
rsync -av  --include '*/' --include-from nuovi_run_michele_t0_m0.00075.rsync --exclude '*' --prune-empty-dirs $MIRROR/run_t0  ./
echo "T>0 new runs"
rsync -av  --include '*/' --include-from nuovi_run_michele.rsync   --exclude '*' --prune-empty-dirs $MIRROR/nuovi_run ./
echo "T>0 old runs"
rsync -av  --include '*/' --include-from vecchi_run_giorgio.rsync --exclude '*'  --prune-empty-dirs $MIRROR/vecchi_run  ./
echo "Galileo runs:"
rsync -av  --include '*/' --include-from runs_galileo_fnegro00.rsync --exclude '*' --prune-empty-dirs $MIRROR/runs_galileo ./
echo "MHRW results..."
rsync -av --prune-empty-dirs $MIRROR/MHRW_results  ./

