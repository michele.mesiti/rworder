#!/bin/bash
COKA=coka       # ssh alias!check your .ssh/config
GALILEO=galileo # ssh alias! check your .ssh/config

FLAGS='--no-perms --no-owner --no-group --omit-dir-times'

umask 002

echo "T=0 runs"
rsync -av $FLAGS --include '*/' --include-from nuovi_run_michele_t0.rsync --exclude '*' --prune-empty-dirs $COKA:/home/mmesiti/OPENACCREPO/build_32_48/run/  ./run_t0
echo "T=0 m=0.00075 runs"
rsync -av $FLAGS --include '*/' --include-from nuovi_run_michele_t0_m0.00075.rsync --exclude '*' --prune-empty-dirs $COKA:/home/mmesiti/run_32_48_4_supplementary  ./run_t0
echo "T>0 new runs"
rsync -av $FLAGS --include '*/' --include-from nuovi_run_michele.rsync --exclude '*' --prune-empty-dirs $COKA:/home/mmesiti/OPENACCREPO_NEWCODE/  ./nuovi_run
echo "T>0 old runs"
rsync -av $FLAGS --include '*/' --include-from vecchi_run_giorgio.rsync --exclude '*' --prune-empty-dirs $COKA:/home/gsilvi/OPENACCREPO/build/run/  ./vecchi_run 
echo "Galileo runs:"
rsync -av $FLAGS --include '*/' --include-from runs_galileo_fnegro00.rsync --exclude '*' --prune-empty-dirs $GALILEO:/galileo/home/userexternal/fnegro00/roberge_weiss  ./runs_galileo

