###
### DIRECTIVES
###

#set terminal wxt #interactive
#set terminal x11 font 'times' #interactive
#set terminal epscairo rounded color font 'times-new-roman'
#set terminal postscript eps rounded color font 'Times-Roman'

set termoption enhanced
set termoption linewidth 1
set termoption fontscale 0.75
set pointsize 2
set samples 1000

set key top left Left reverse width -1.0
set xlabel offset +0.0,+0.75
set ylabel offset +2.5,+0.00
set xtics offset +0.0,+0.5
set ytics offset +0.5,+0.0

###
### COLOR PALETTE; ALL DEFINED AS SQUARE DOTS AND SOLID LINES
###

set style line 1 linetype 1 linewidth 4 pointtype 7 linecolor rgb '#000000' #black
set style line 2 linetype 1 linewidth 4 pointtype 7 linecolor rgb '#FF0000' #red
set style line 3 linetype 1 linewidth 4 pointtype 7 linecolor rgb '#00FF00' #green
set style line 4 linetype 1 linewidth 4 pointtype 7 linecolor rgb '#0000FF' #blue
set style line 5 linetype 1 linewidth 4 pointtype 7 linecolor rgb '#FFFF00' #yellow
set style line 6 linetype 1 linewidth 4 pointtype 7 linecolor rgb '#BC8F0E' #brown
set style line 7 linetype 1 linewidth 4 pointtype 7 linecolor rgb '#DCDCDC' #grey
set style line 8 linetype 1 linewidth 4 pointtype 7 linecolor rgb '#9400D3' #violet
set style line 9 linetype 1 linewidth 4 pointtype 7 linecolor rgb '#00FFFF' #cyan
set style line 10 linetype 1 linewidth 4 pointtype 7 linecolor rgb '#FF00FF' #magenta
set style line 11 linetype 1 linewidth 4 pointtype 7 linecolor rgb '#FFA500' #orange
set style line 12 linetype 1 linewidth 4 pointtype 7 linecolor rgb '#7221BC' #indigo
set style line 13 linetype 1 linewidth 4 pointtype 7 linecolor rgb '#670748' #maroon
set style line 14 linetype 1 linewidth 4 pointtype 7 linecolor rgb '#40E0D0' #turquoise
set style line 15 linetype 1 linewidth 4 pointtype 7 linecolor rgb '#008B00' #web-green


###
### LINEWIDTH 2 POINTtype LIST ( N is full, N-1 is empty dot)
###

# 05 square
# 07 circle
# 09 triangle-up
# 11 triangle-down
# 13 diamond
# 15 penta
# 02 cross


###
###LINETYPE LIST
###

# 01 solid
# 02 dashed
# 03 dashed light
# 04 dotted
