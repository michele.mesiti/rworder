####                          ##
### CHIRAL SUSC MAXIMA PLOTS ###
##                          ####

ROOTMAXIMA='../Analisi/maxima/'


DATA=ROOTMAXIMA.'mass0.00075.set.out.dat'

#set terminal postscript eps enhanced color rounded font 'Times-Roman,26pt'

set terminal epslatex color
load 'style.gp'

set key top left

set out 'lightSuscMaxima_m00075.tex'
set yrange [9:15]
set xrange [12:32]
set xlabel '$N_s$'
set ylabel '$\chi_{\bar{\psi}\psi, max}$'
plot DATA  u 1:2:3  w e ls 4 ti '$am_l = 0.00075$'



