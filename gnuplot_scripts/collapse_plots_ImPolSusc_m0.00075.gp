####                ##
### COLLAPSE PLOTS ###
##                ####

ROOTCOLLAPSE='../Analisi/collapses/'

MASS='00075'


DATA16=ROOTCOLLAPSE.'analisi_settings_m0.'.MASS.'_L16.set.out.dat_imPolsusc_Collapse.dat'
DATA20=ROOTCOLLAPSE.'analisi_settings_m0.'.MASS.'_L20.set.out.dat_imPolsusc_Collapse.dat'
DATA24=ROOTCOLLAPSE.'analisi_settings_m0.'.MASS.'_L24.set.out.dat_imPolsusc_Collapse.dat'
DATA28=ROOTCOLLAPSE.'analisi_settings_m0.'.MASS.'_L28.set.out.dat_imPolsusc_Collapse.dat'


DATA16RW=ROOTCOLLAPSE.'analisi_settings_m0.'.MASS.'_L16.set.MHRWout.gauge.dat_imPolsusc_RWCollapse.dat'
DATA20RW=ROOTCOLLAPSE.'analisi_settings_m0.'.MASS.'_L20.set.MHRWout.gauge.dat_imPolsusc_RWCollapse.dat'
DATA24RW=ROOTCOLLAPSE.'analisi_settings_m0.'.MASS.'_L24.set.MHRWout.gauge.dat_imPolsusc_RWCollapse.dat'
DATA28RW=ROOTCOLLAPSE.'analisi_settings_m0.'.MASS.'_L28.set.MHRWout.gauge.dat_imPolsusc_RWCollapse.dat'

#set terminal postscript eps enhanced color rounded font 'Times-Roman,26pt'

set terminal epslatex color
load 'style.gp'

set key top left

set out 'ImPolSusc1st_m'.MASS.'.tex'
set xlabel '$(\beta-\beta_c) N_s^3$'
set ylabel '$\chi_{|\textrm{Im P}|} / N_s^3 $'
set xrange [-200:200]
plot DATA28RW u 1:($2-$3):($2+$3) w filledcu fs solid 0.5 border not linecolor rgb '#00FF00',\
     DATA16RW u 1:($2-$3):($2+$3) w filledcu fs solid 0.5 border not linecolor rgb '#000000',\
     DATA20RW u 1:($2-$3):($2+$3) w filledcu fs solid 0.5 border not linecolor rgb '#FF0000',\
     DATA24RW u 1:($2-$3):($2+$3) w filledcu fs solid 0.5 border not linecolor rgb '#0000FF',\
     DATA16 u 1:2:3  w e ls 1 ti '$N_s=16$',\
     DATA20 u 1:2:3  w e ls 2 ti '$N_s=20$',\
     DATA24 u 1:2:3  w e ls 4 ti '$N_s=24$',\
     DATA28 u 1:2:3  w e ls 6 ti '$N_s=28$'
     

set out 'ImPolSuscIs_m'.MASS.'.tex'
set xlabel '$(\beta-\beta_c) N_s^{1.587}$'
set ylabel '$\chi_{|\textrm{Im P}|} / N_s^{1.963}$'
set xrange [-2:2]
plot DATA28RW u 4:($5-$6):($5+$6) w filledcu fs solid 0.5 border not linecolor rgb '#00FF00',\
     DATA16RW u 4:($5-$6):($5+$6) w filledcu fs solid 0.5 border not linecolor rgb '#000000',\
     DATA20RW u 4:($5-$6):($5+$6) w filledcu fs solid 0.5 border not linecolor rgb '#FF0000',\
     DATA24RW u 4:($5-$6):($5+$6) w filledcu fs solid 0.5 border not linecolor rgb '#0000FF',\
     DATA16 u 4:5:6  w e ls 1 ti '$N_s=16$',\
     DATA20 u 4:5:6  w e ls 2 ti '$N_s=20$',\
     DATA24 u 4:5:6  w e ls 4 ti '$N_s=24$',\
     DATA28 u 4:5:6  w e ls 6 ti '$N_s=28$'
     
