####                       ##
### BINDER CUMULANT PLOTS ###
##                       ####

ROOTANALYSIS='../Analisi/'

DATA4C=ROOTANALYSIS.'analisi_settings_m0.00075_L20.set.out.dat'
DATA3C=ROOTANALYSIS.'pedagogic_bias/analisi_settings_m0.00075_L20.set.3c.out.dat'
DATA2C=ROOTANALYSIS.'pedagogic_bias/analisi_settings_m0.00075_L20.set.2c.out.dat'

v4=20*20*20*4
xShift=0.001

#set terminal postscript eps enhanced color rounded font 'Times-Roman,26pt'

set terminal epslatex color
load 'style.gp'

set key top left

set out 'absImNumBar2.tex'
set xlabel '$\beta$'
set ylabel '$|\textrm{Im } n_l|$'
set xrange [3.31:3.38]
set yrange [0:6.4]
plot DATA4C u 1:(v4*$8**2):(v4*2*$9*$8) w e ls 1 ti '$\langle|\textrm{Im}n_l|\rangle^2\,,n_\eta=4$',\
     DATA3C u 1:(v4*$8**2):(v4*2*$9*$8) w e ls 2 ti '$\langle|\textrm{Im}n_l|\rangle^2\,,n_\eta=3$',\
     DATA2C u 1:(v4*$8**2):(v4*2*$9*$8) w e ls 4 ti '$\langle|\textrm{Im}n_l|\rangle^2\,,n_\eta=2$',\
     DATA4C u 1:($12):($13)  w e ls 6 ti '$\langle($Im $n_l)^2\rangle\,,n_\eta=4$'
 


