####                       ##
### BINDER CUMULANT PLOTS ###
##                       ####

ROOTANALYSIS='../Analisi/'


# MASS='00075'
# MASS='0015'
# MASS='003'



DATA16=ROOTANALYSIS.'analisi_settings_m0.'.MASS.'_L16.set.out.dat'
DATA20=ROOTANALYSIS.'analisi_settings_m0.'.MASS.'_L20.set.out.dat'
DATA24=ROOTANALYSIS.'analisi_settings_m0.'.MASS.'_L24.set.out.dat'

DATA16RW=ROOTANALYSIS.'analisi_settings_m0.'.MASS.'_L16.set.MHRWout.gauge.dat'
DATA20RW=ROOTANALYSIS.'analisi_settings_m0.'.MASS.'_L20.set.MHRWout.gauge.dat'
DATA24RW=ROOTANALYSIS.'analisi_settings_m0.'.MASS.'_L24.set.MHRWout.gauge.dat'

#set terminal postscript eps enhanced color rounded font 'Times-Roman,26pt'

set terminal epslatex color
load 'style.gp'

set key top right

set out 'imPolBinder'.MASS.'.tex'
set xlabel '$\beta$'
set ylabel '$B_4(\beta)$'
set xrange [BMIN:BMAX]
plot DATA16RW u 1:($8-$9):($8+$9) w filledcu fs solid 0.5 border not linecolor rgb '#000000',\
     DATA20RW u 1:($8-$9):($8+$9) w filledcu fs solid 0.5 border not linecolor rgb '#FF0000',\
     DATA24RW u 1:($8-$9):($8+$9) w filledcu fs solid 0.5 border not linecolor rgb '#0000FF',\
     DATA16 u 1:18:19  w e ls 1 ti '$N_s=16$',\
     DATA20 u 1:18:19  w e ls 2 ti '$N_s=20$',\
     DATA24 u 1:18:19  w e ls 4 ti '$N_s=24$',\
     1.604 linecolor rgb '#000000' linewidth 1 dashtype 2 not

