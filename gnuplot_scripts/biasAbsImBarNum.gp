####                       ##
### BINDER CUMULANT PLOTS ###
##                       ####

ROOTANALYSIS='../Analisi/'

DATA4C=ROOTANALYSIS.'analisi_settings_m0.00075_L20.set.out.dat'
DATA3C=ROOTANALYSIS.'pedagogic_bias/analisi_settings_m0.00075_L20.set.3c.out.dat'
DATA2C=ROOTANALYSIS.'pedagogic_bias/analisi_settings_m0.00075_L20.set.2c.out.dat'


#set terminal postscript eps enhanced color rounded font 'Times-Roman,26pt'

set terminal epslatex color
load 'style.gp'

set key top left

set out 'absImNumBar.tex'
set xlabel '$\beta$'
set ylabel '$|\textrm{Im } n_l|$'
plot DATA4C u 1:8:9  w e ls 1 ti '$n_\eta = 4$',\
     DATA3C u 1:8:9  w e ls 2 ti '$n_\eta = 3$',\
     DATA2C u 1:8:9  w e ls 4 ti '$n_\eta = 2$'


