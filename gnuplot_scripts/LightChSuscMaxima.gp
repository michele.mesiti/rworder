####                          ##
### CHIRAL SUSC MAXIMA PLOTS ###
##                          ####

ROOTMAXIMA='../Analisi/maxima/'


DATA3=ROOTMAXIMA.'mass0.003.set.out.dat'
DATA15=ROOTMAXIMA.'mass0.0015.set.out.dat'

#set terminal postscript eps enhanced color rounded font 'Times-Roman,26pt'

set terminal epslatex color
load 'style.gp'

set key top left

set xrange [12:28]
set out 'lightSuscMaxima.tex'
set xlabel '$N_s$'
set ylabel '$\chi^{r}_{\bar{\psi}\psi, max}$'
plot DATA3  u 1:2:3  w e ls 1 ti '$am_l = 0.003$',\
     DATA15 u 1:2:3  w e ls 2 ti '$am_l = 0.0015$',



