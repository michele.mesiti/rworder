#!/usr/bin/env python3
from sys import argv
import imageio

imageNames = argv[2:]
images = []

for imageName in sorted(imageNames):
    images.append(imageio.imread(imageName))

imageio.mimsave(argv[1],images,duration = 1.5)




