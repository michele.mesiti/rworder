#!/bin/bash

ROOT=$1
RESULTS=$2
destdir=$(basename $ROOT)
dirs=$(ls -d $ROOT/3.* ) 
#creating gnuplot script
echo 'set terminal png' > filmino.auto.gp
echo 'set out "temp"' >> filmino.auto.gp
echo 'plot 0' >> filmino.auto.gp

echo 'set terminal png' > filminores.auto.gp
echo "set out 'resslide0.png'" >> filminores.auto.gp
echo 'plot "'$RESULTS'" u 1:16:17 w e t "Chi_|ImP|"' >> filminores.auto.gp
for dir in $dirs:
do
  beta=$(basename $dir)
  
  if [ "$(ls $dir/*/gauge*[0-9])" ]
  then 
     echo "print \"creating plot, beta $beta\"" >> filmino.auto.gp
     tottraj=$(wc -l $dir/*/gauge*[0-9] | grep total | awk '{print $1}' )
     echo Tot $tottraj
     echo "set out 'slide"$beta".png'" >> filmino.auto.gp
     echo 'replot "<(cat' $dir'/*/gauge*[0-9])" u 1:6 t' "'$beta, traj $tottraj' " >> filmino.auto.gp

     echo "set out 'resslide"$beta".png'" >> filminores.auto.gp
     echo 'replot "<(awk '"'"'($1<=' $beta '+0.001){print $0}'"'" $RESULTS')" u 1:16:17 w e not' >> filminores.auto.gp



  else 
      echo No gaugefile found in $dir/*/gauge*[0-9]
  fi

done 


gnuplot filmino.auto.gp
gnuplot filminores.auto.gp

echo Assembling slides...
./assemble.py filmino.gif slide*.png
./assemble.py filminores.gif resslide*.png
rm slide*.png
echo cp filmino.gif /afs/pi.infn.it/user/mesiti/public_html/robby/$destdir/
cp filmino.gif /afs/pi.infn.it/user/mesiti/public_html/robby/$destdir/
echo cp filminores.gif /afs/pi.infn.it/user/mesiti/public_html/robby/$destdir/
cp filminores.gif /afs/pi.infn.it/user/mesiti/public_html/robby/$destdir/
echo cp resslide0.png /afs/pi.infn.it/user/mesiti/public_html/robby/$destdir/
cp resslide0.png /afs/pi.infn.it/user/mesiti/public_html/robby/$destdir/

